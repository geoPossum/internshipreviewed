$(document).ready(function(){
    jQuery(".list-item a").click(function (event){
        event.preventDefault();
        var url = jQuery(this).attr("href");
        jQuery.getJSON(url, function(data, status){
            var course = data[0]["course"];
            var companyName = data[0]["companyName"];
            var location = data[0]["location"];
            var pay = data[0]["pay"];
            var skills = data[0]["skills"];
            var experience = data[0]["experience"];
            var md5 = data[0]["md5"];
            jQuery(".edit-right .course input").attr("value", course);
            jQuery(".edit-right .companyName input").attr("value", companyName);
            jQuery(".edit-right .location input").attr("value", location);
            jQuery(".edit-right .pay input").attr("value", pay);
            jQuery(".edit-right .skills input").attr("value", skills);
            jQuery(".edit-right .experience textarea").html(experience);
            jQuery(".edit-right .checkValue input").attr("value", md5);
        });
    });
    
    jQuery("form").bind("submit",function(event){
        event.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr("action");
        jQuery.post(url, {"data": data}, 
            function(data2, status){
                console.log(data2);
                window.location.assign("/index.php/mydata/status/"+status);
            }
        );
    });
});