<?php

$config["facebook_app_id"] = "1391629301119712";
$config["facebook_app_secret"] = "1c080bae7a49c9dde6e01d6ce997419c";
$config["facebook_redirect_uri"] = "http://www.launchform.com/index.php/access/facebookcallback";
$config["facebookURL"] = "https://www.facebook.com/dialog/oauth?".
	"client_id=".$config["facebook_app_id"].
	"&redirect_uri=".$config["facebook_redirect_uri"].
	"&response_type=code".
	"&scope=email";

$config["accessTokenURL"] = "https://graph.facebook.com/v2.4/oauth/access_token?".
	"client_id=".$config["facebook_app_id"].
	"&redirect_uri=".$config["facebook_redirect_uri"].
	"&client_secret=".$config["facebook_app_secret"].
	"&code=";

$config["appAccessToken"] = "https://graph.facebook.com/v2.4/oauth/access_token?".
	"client_id=".$config["facebook_app_id"].
	"&client_secret=".$config["facebook_app_secret"].
	"&grant_type=client_credentials";

$config["validateTokenURL"] = "https://graph.facebook.com/debug_token?input_token=";

$config["fBMeEndpoint"] = "https://graph.facebook.com/me?access_token=";
        
$config["siteTitle"] = "Launchform";
?>