<?php
class MyValues {
	// numbers must be arranged in the way that division of values will follow from left being lowest
	// to right being highest
	var $company_size = array( "Small approx. 20p or less"," 20-100p", "Large,Greater than 100");
	var $days_of_working = array("Random", "7 days" ,  "Monday-Friday",  "Monday-Saturday");
	var $dress_code = array("Smart Casual" ,"Free Style", "Formal");
	var $hands_on = array("Mostly Watched","Did Average Tasks","Very Hands On");
	var $interesting = array("Very Boring","Not Interesting","Average","Very Interesting");
	var $redo =array("No","Yes");
}

?>