<?php
class Access_model extends CI_Model{
	public function __construct()
	{
            $this->load->database();
	}
        
        public function insertFBUpdateUser($data){
            $this->db->select("userID");
            if ( array_key_exists("email", $data) ){
                $query = $this->db->get_where("users", array("email" => $data["email"]));
            }
            else{
                $query = $this->db->get_where("users", array("userID" => $data["fbId"]));
            }
            $results = $query->result_array();
            if (empty($results)){
                $this->db->insert('users', $data); 
                $this->db->select("userID");
                if ( array_key_exists("email", $data) ){
                    $query = $this->db->get_where("users", array("email" => $data["email"]));
                }
                else{
                    $query = $this->db->get_where("users", array("userID" => $data["fbId"]));
                }
                $results = $query->result_array();
            }
            if (!empty($results)){
                return $results[0]["userID"];
            }
            else{
                return null;
            }
        }

	//only used by facebook and google logins and forgot password
	public function valid_user_email($email){
		$stored_hash = $this->db->query("select * from myusers where email='$email'");
		$row = $stored_hash->row();
		if ($row){
			return true;
		}
		else{
			return 0;
		}
	}

	//used by survey email
	public function valid_survey_email($email){
		$stored_hash = $this->db->query("select * from surveyTable where email='$email'");
		$row = $stored_hash->row();
		if ($row){
			return true;
		}
		else{
			return 0;
		}
	}

	public function new_user($email, $priviledge=1){
		$token = md5(uniqid(mt_rand(), true));
		$data = array(
			"email" => $email,
			"priviledge" => $priviledge,
			"token" => $token
			);

		//only insert if there is no previous user
		$stored_hash = $this->db->query("select * from myusers where email='$email'");
		$row = $stored_hash->row();
		if (!$row){
			$this->db->insert('myusers', $data); 
		}
		return;

	}
	
        public function isAdmin($userID){
		$this->db->where("userID", $userID);
                $this->db->where("level >", 1);  
                $query = $this->db->get("users");
                return $query->num_rows();
	}

	public function get_properties($email){
		// without login, limit number of views per page to 3, login to see more
		// add priviledge level, default 1
		// sort by pay when priviledge level 2
		// get priviledge level 2 when invite someone or post on facebook
		$query = $this->db->query("select priviledge from myusers where email='$email'");
		$row = $query->row();
		if ($row){
			return $row->priviledge;
		}
		else{
			return 0;
		}
	}

	public function get_companies($field=""){
		$email = $this->session->userdata("useremail");
		$this->db->select("company_name,company_slug");
		$query = $this->db->get_where("surveyTable", array("email"=>$email) );
		return $query->result_array();
	}

	public function get_data($company_slug,$course=""){
		$email = $this->session->userdata("useremail");
		if ( $course ){
			$this->db->select($course);
		}
		$query = $this->db->get_where("surveyTable", array("email"=>$email, "company_slug"=>$company_slug) );	
		return $query->result_array();
	}
	
        public function update($company_slug,$id,$email,$data){
		$this->db->where('id', $id);
		$this->db->where('company_slug', $company_slug);
		$this->db->where('email', $email);
		$this->db->update("surveyTable",$data );
	}

	public function update_priviledge($email){
		//get priviledge
		$this->db->select("priviledge");
		$query = $this->db->get_where('myusers', array("email"=> $email));
		$result = $query->result_array();
		$priviledge = $result[0]["priviledge"];

		$data["priviledge"] = $priviledge+1;
		$this->db->where('email', $email);
		$this->db->where('priviledge', $priviledge);
		$this->db->update("myusers",$data );
		return $priviledge+1;
	}

	public function valid_token($token,$email){
		$this->db->select("token");
		$query = $this->db->get_where('myusers', array("email"=> $email));
		$row = $query->row();
		if ($row){
			if ($row->token == $token){
				return 1;
			}
		}
		return 0;
	}
	
        public function get_token_for_email($email){
		$this->db->select("token");
		$query = $this->db->get_where('myusers', array("email"=> $email));
		$row = $query->row();
		if ($row){
			return $row->token;
		}
		return 0;
	}
	
        public function update_token_and_password($email, $password){
		$token = md5(uniqid(mt_rand(), true));
		$hashed_password = $this->encrypt->sha1($password);

		$data = array(
			'token' => $token,
			'password' => $hashed_password,
			);
		$this->db->where('email', $email);
		$this->db->update("myusers",$data );
		return 1;
	}
}
?>