<?php
class MySort{
	function sort( $a, $b){
		return $a["pay"] < $b["pay"];
	}
}

class Attachments_model extends CI_Model{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_courses($company_name = FALSE){
		//get a list of all courses at all companies
		$this->db->distinct();
		if ($company_name == FALSE){
                    $this->db->select('courseSlug, course');
			$query = $this->db->get_where("surveyTable",array('showInSite'=>1));
			
		}
                else{
		//get a list of where company is this
                    $this->db->select('SubmissionID, courseSlug, course');
                    $query = $this->db->get_where('surveyTable', array("companySlug" => $company_name,
                        'showInSite'=>1),10);
                }
                $ra = $query->result_array();
                $result = array();
                $this->db->distinct();
                foreach($ra as $r){
                    $query = $this->db->get_where('surveyTable', array("courseSlug" => $r['courseSlug'], 'showInSite'=>1));
                    $num = $query->num_rows();
                    $r["noCoursesListed"] = $num;
                    array_push($r, array("noCoursesListed" => $num));
                    array_push($result, $r);
                }
		return $result;
	}

	public function get_companies($courseSlug = FALSE){
		//get a list of all companies
		$this->db->distinct();
		$this->db->select('companyName, companySlug ');
		if ($courseSlug == FALSE){
			$query = $this->db->get_where("surveyTable",array('showInSite'=>1));
		}
		else{
			$query = $this->db->get_where('surveyTable',array('courseSlug'=>$courseSlug,
                            'showInSite'=>1));
		}
//                return $query->result_array();

		$return = $temp = array();
		//return is an empty array

		foreach ($query->result_array() as $item){
			//replace characters and reassign
			$this->db->select("pay, location, SubmissionID,");
			$query2 = $this->db->get_where('surveyTable',array('companySlug'=>$item["companySlug"]));
			$answer = $query2->result_array();
			$item["pay"] = $answer[0]["pay"];
			$item["pay"] = preg_replace( "/[a-z]|\/|\.|\,|=|\s/", "",strtolower( $item["pay"] ) );
			$item["pay"] = preg_match("/[0-9]*/", $item["pay"], $match);
			$item["pay"]  = $match[0];
			$item["pay_slug"] = $item["pay"];
			if ( $item["pay"]=="" ){
				$item["pay"] = "0";
			}
                        $item['location'] = $answer[0]['location'];
                        $item['SubmissionID'] = $answer[0]['SubmissionID'];
			array_push($return, $item);
		}

		//sort by pay
		usort( $return, array( "MySort" , "sort") );

		// add necessary commas
		foreach ($return as $item){
			$length = strlen( $item["pay"] );
			if ( $length > 3 ) {
				$item["pay"] = substr($item["pay"], 0, $length - 3).",".substr($item["pay"],  $length - 3, $length);
			}

			$item["pay"] = "Ksh. ".$item["pay"]."/=";

				// unset($item["pay"]);
				// unset($item["pay_slug"]);
 			array_push($temp, $item);
		}

		$return = $temp;

		return $return;
	}

	public function getDataForSubmissionID($submissionID){
           $query = $this->db->get_where("surveyTable", array("SubmissionID" => $submissionID));
           return $query->result_array();
        }
        
        public function getExperience($courseSlug, $companySlug){
            $this->db->distinct();
            $this->db->select('skills, experience');
            $query = $this->db->get_where("surveyTable", array(
                "courseSlug" => $courseSlug,
                "companySlug" => $companySlug,
                ));
            return $query->result_array();
        }
        
	public function save_contact( $input ){
            $this->db->insert('formContact', $input); 
	}
        
        public function new_submission($data){
            $this->db->insert('surveyTable', $data); 
            $this->db->select_max("SubmissionID");
            $id = $this->db->get('surveyTable');
            $row = $id->row();
            return $row->SubmissionID;
        }
        
        public function setUserIDForSubmission($id, $userID){
            $this->db->update('surveyTable', array("userID" => $userID ), array("SubmissionID" => $id) );
        }
        
        public function getTopRatedCompanies(){
            $this->db->distinct();
            $this->db->select('companyName, companySlug');
            $this->db->order_by("internalRating", 'desc');
//            $this->db->where("internalRating >", 0);
            $this->db->limit(3);
            $query = $this->db->get('surveyTable');
            $results_array = $query->result_array();
            $temp = array();
            foreach( $results_array as $result){
                $this->db->select('location, course');
                $query2 = $this->db->get_where('surveyTable', 
                        array("companySlug" => $result['companySlug']));
                $result2 = $query2->result_array();
                $result["location"] = $result2[0]['location'];
                $result["courses"] = array();
                foreach($result2 as $r){
                    array_push($result["courses"], $r["course"]);
                }
                array_push($temp,$result);
            }
            return $temp;
        }
}
?>
