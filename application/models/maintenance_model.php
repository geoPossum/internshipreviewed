<?php
class Maintenance_model extends CI_Model{
    public function __construct()
    {
            $this->load->database();
            $this->load->model('access_model');
    }

    public function createSlugs(){
            $changed = array(); //holds values that have been changed
            $this->db->distinct();
            $this->db->select('course');
            $query = $this->db->get("surveyTable");
            $data_to_change = array();

            foreach($query->result_array() as $k){
                    $course_name = $k["course"];
                    $courseSlug = strtolower(url_title($course_name,"-",TRUE) );
                    array_push($data_to_change, array('course' => $course_name,'courseSlug' => $courseSlug) );
            }

            $this->db->update_batch('surveyTable', $data_to_change,'course'); 

            array_push($changed, $data_to_change);
            //change companies
            $this->db->distinct();
            $this->db->select('companyName');
            $query = $this->db->get("surveyTable");
            $data_to_change = array();

            foreach($query->result_array() as $k){
                    $companyName = $k["companyName"];
                    $companySlug = strtolower(url_title($companyName,"-",TRUE) );
                    array_push($data_to_change, array('companyName' => $companyName,'companySlug' => $companySlug) );
            }

            $this->db->update_batch('surveyTable', $data_to_change,'companyName'); 
            array_push($changed, $data_to_change);
            return $changed;
    }

    public function run_script($text){
            $query = $this->db->query($text);
            if ( gettype($query) != "boolean"){
                    if ($query->num_rows() > 0){
                            return $query->result_array();
                    }
            }
            else{
                    return array(array("Result" => "<br>No Result"));
            }
    }

    public function get_unseen(){
            $this->db->select('SubmissionID, companyName, course');
            $query = $this->db->get_where( 'surveyTable', array('seen'=>'0') );
            return $query->result_array();
    }

    public function get_unseen_user_data($entry_id){
            $this->db->select('id, email, companyName, companySlug, company_location,calendar_year_attached,academic_year_attached,course,company_size,
                    maximum_duration,working_days,pay,dress_code,nature_of_work,skills,testimony,hands_on,interesting,innovation,redo');
            $query = $this->db->get_where( 'surveyTable', array("id" => $entry_id, 'seen'=>'0') );

            $data = $query->result_array();
            if (!$data){
                    //query was unsuccessful
                    return false;
            }
            $interesting_value = $redo_value = $hands_on_value = 0;

            foreach ($data as $value) {
                    $interesting_value += $value["interesting"];
                    $redo_value+= $value["redo"];
                    $hands_on_value += $value["hands_on"];
            }

            $interesting_value = ceil(($interesting_value*100)/count($data)) ;
            $redo_value = ceil(($redo_value*100)/count($data)) ;
            $hands_on_value = ceil(($hands_on_value*100)/count($data)) ;

            $data["interesting_value"] = $interesting_value;
            $data["redo_value"] = $redo_value;
            $data["hands_on_value"] = $hands_on_value;

            return $data;
    }

    public function validate($id){
            $data = array( 'seen' => '1' );
            $this->db->update("surveyTable",$data,array("seen"=>'0',"id"=>$id) );
            return;
    }

    public function delete_entry($id){
            $this->db->delete('surveyTable', array('id' => $id)); 
            return;
    }

    public function updateCompanyName($former_name, $new_name){
            $data["companyName"] = $new_name;
            $this->db->update("surveyTable",$data,array("companyName"=>"$former_name"));
    }

    public function updateCourseName($former_name, $new_name){
            $data["course"] = $new_name;
            $this->db->update("surveyTable",$data, array("course"=>"$former_name"));
    }

    public function get_user_data($course=FALSE,$companyName=FALSE){
            // $query = $this->db->query( "select * from surveyTable");
            $this->db->select("id");
            $query = $this->db->get_where( 'surveyTable', array() );
            return $query->result_array();
    }

    public function get_contacted(){
            $query = $this->db->query( "select * from formContact where resolved='0'");
            return $query->result_array();
    }

    public function getSurveyDataForID($id){
            $query = $this->db->get_where( 'surveyTable', array("SubmissionID"=>$id) );
            return $query->result_array();
    }

    public function updateSurveyDataWithIDs( $SubmissionID, $data){
            $this->db->update("surveyTable",$data, 
                array("SubmissionID" => $SubmissionID) 
                );
            return $data;
    }

    public function insert_survey_data( $data ){
            $str = $this->db->insert_string("surveyTable",$data);
            $query = $this->db->query( $str );
            return $query;
    }

    public function insertSurveyBatch($uploads){
        foreach($uploads as $upload){
            $email = $upload["email"];
            unset($upload["email"]);
            $id = null;
            if (!empty($email)){
                $id = $this->access_model->insertFBUpdateUser(array("email" => $email) );
            }
            $upload["userID"] = $id;
            $upload["courseSlug"] = strtolower(url_title($upload["course"],"-",TRUE) );
            $upload["companySlug"] = strtolower(url_title($upload["companyName"],"-",TRUE) );
            $this->db->insert("surveyTable", $upload);
        }      
    }
    
    public function getSlugForCourseName($course){
        $this->db->select("courseSlug, course");
        $this->db->distinct();
        $q = $this->db->get_where("surveyTable", array("course" => $course));
        $x = $q->result_array();
        return $x[0];
    }
    
    public function getSlugForCompanyName($company){
        $this->db->select("companyName, companySlug");
        $this->db->distinct();
        $q = $this->db->get_where("surveyTable", array("companyName" => $company));
        $x = $q->result_array();
        return $x[0];
    }
    
    public function validateAll(){
        $this->db->where("seen", 0);
        $data["seen"] = 1;
        $this->db->update("surveyTable", $data);
    }
    
    public function markContactRead($id){
        $this->db->where("contactID", $id);
        $data["resolved"] = 1;
        $this->db->update("formContact", $data);
    }
}
?>