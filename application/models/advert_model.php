<?php
class Advert_model extends CI_Model{
    public function __construct()
    {
        $this->load->database();
    }
    public function getHomePageAds(){
        $this->db->limit(3);
        $today = strftime("%Y-%m-%d", gmmktime());
        $this->db->order_by("pay", "desc"); 
        $this->db->select("adverts.advertID, advertisingCompanies.companyName, advertisingCompanies.location, "
                . "adverts.courses, advertFeesHistory.datePaid, advertFeesHistory.duration");
        $this->db->from('advertisingCompanies');
        $this->db->join('advertFeesHistory', 'advertisingCompanies.companyName = advertFeesHistory.companyName', 'inner');
        $this->db->join('adverts', 'advertisingCompanies.companyName = adverts.companyName', 'inner');
        $query = $this->db->get();
        $temp = array();
        foreach( $query->result_array() as $result){
            $return = $result;
            $courses = $return["courses"];
            $return["courses"] = array();
            foreach(explode(",", $courses) as $course ){
                array_push($return["courses"], $course);
            }
            //check for payment duration as well
            $endDate = strtotime($return['datePaid']." + ".$return["duration"].'days + 24 hours');
//            $temp["extra"] = strftime("%c",$endDate);
//            array_push($temp, $return);
            if ($endDate >= gmmktime() ){
                array_push($temp, $return);
            }
        }
        return $temp;
    }
    
    public function getAd($advertID){
        $this->db->join('advertisingCompanies', 'advertisingCompanies.companyName = adverts.companyName', 'inner');
        $query  = $this->db->get_where('adverts', array("advertID" => $advertID));
        return $query->result_array();
    }
    
    public function getAllAds(){
        $this->db->distinct();
        $this->db->order_by("adverts.advertID", "desc");
        $this->db->select("advertFeesHistory.datePaid, duration ,adverts.advertID, adverts.companyName, advertisingCompanies.location");
        $this->db->join('advertisingCompanies', 'advertisingCompanies.companyName = adverts.companyName', 'inner');
        $this->db->join('advertFeesHistory', 'advertisingCompanies.companyName = advertFeesHistory.companyName', 'inner');
        $query  = $this->db->get('adverts');
        $temp = array();
        foreach($query->result_array() as $result){
            $this->db->distinct();
            $this->db->limit(1);
            $this->db->select("pay");
            $query2  = $this->db->get_where('adverts', array("companyName" => $result["companyName"]));
            $temp2 = $query2->result_array();
            $result["pay"] = $temp2[0]["pay"];
            $endDate = strtotime($result['datePaid']." + ".$result["duration"].'days + 24 hours');
            if ($endDate >= gmmktime() ){
                array_push($temp, $result);
            }
            }
        return $temp;
    }
}