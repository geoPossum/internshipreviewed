<?php
class Mydata_Model extends CI_Model{
	public function __construct()
	{
		$this->load->database();
	}
        public function getCompanyCourseSubIDForUser($id){
            $this->db->distinct();
            $this->db->select("SubmissionID, companyName, course");
            $query = $this->db->get_where("surveyTable", array("userID" => $id));
            return $query->result_array();
        }
        public function getDataforSubmissionID($userID, $submissionID){
            $data = array(
                "md5" => md5(random_string())
            );
            $this->db->update('surveyTable', $data, array(
                "userID" => $userID,
                "SubmissionID" => $submissionID
                )); 
            
            $query = $this->db->get_where("surveyTable", array(
                "userID" => $userID,
                "SubmissionID" => $submissionID
                ));
            return $query->result_array();
        }
        public function updateDataForMd5($data){
            $md5 = $data["checkValue"];
            unset($data["checkValue"]);
            $this->db->update('surveyTable', $data, array(
                "md5" => $md5
                ));
        }
}