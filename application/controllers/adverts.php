<?php
class Adverts extends CI_Controller{
    public function __construct()
	{
		parent::__construct();
                $this->load->model('advert_model');
		$this->load->library('session');
                $this->load->helper('url');
                $this->config->load("myvalues");
		$this->load->library('MyValues');
		// $this->output->enable_profiler(true);
	}
    public function index(){
        $data = array("loggedIN" => 0);
        if ($this->session->userdata("userID")){
            $data["loggedIN"] = 1;
        }
        $data["companies"] = $this->advert_model->getAllAds();
        $data["siteTitle"] = $this->config->item("siteTitle");
//        var_dump($data["companies"]);
        $this->load->view("purpleTheme/advertisingCompanies",$data);
    }
    public function seeAd($advertID){
        $data = array("loggedIN" => 0);
        if ($this->session->userdata("userID")){
            $data["loggedIN"] = 1;
        }
        $data["siteTitle"] = $this->config->item("siteTitle");
        $data["item"] = $this->advert_model->getAd($advertID);
        $this->load->view("purpleTheme/adItem",$data);
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

