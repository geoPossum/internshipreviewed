<?php
class Attachments extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('attachments_model');
        $this->load->model('advert_model');
        $this->load->model('access_model');
        $this->load->library('session');
        $this->config->load("myvalues");
        $this->load->library('MyValues');
        // $this->output->enable_profiler(true);
    }
    private function newCaptcha(){
        $n = array();
        $num1 = rand(8, 12);
        $num2 = rand(4, $num1-1);
        $num3 = rand(1, $num2 -1 > 0? $num2-1 : $num2);
        $ops = array(
            array(
                "operator" => "*",
                "text" => " multiply by ",
            ),
            array(
                "operator" => "+",
                "text" => " add ",
            ),
            array(
                "operator" => "-",
                "text" => " minus ",
            )
        );
        $op1 = $ops[array_rand($ops)];
        $op2 = $ops[array_rand($ops)];
        $stat = '$n["answer"]='."$num1 " . $op1['operator'] . " ( $num2 " .
                $op2['operator'] . " $num3 );";
        eval($stat);
        $n["statement"] = "$num1 " . $op1['text'] ."( $num2  " .
                $op2['text'] . " $num3 )";
        return $n;
    }

    private function toSlug($var){
        return strtolower(url_title($var,"-",TRUE) );
    }

    public function index(){
        //sort urls
        $this->load->helper('url');
        $location = "/index.php/";

        // if history array does not exist create it and add current
        // location, else add it only if the current location is not the
        // last entry in the list.
        if (! $this->session->userdata("history") ){
            $history = array();
            array_push($history, $location);
        }
        else{
            $history = $this->session->userdata("history");
            if ( end($history) != $location ){
                array_push($history, $location);
            }
        }
        $this->session->set_userdata("history", $history);
        
        $data = array("loggedIN" => 0);
        $data["admin"] = 0;
        
        $userID = $this->session->userdata("userID");
        if ($userID){
            $data["loggedIN"] = 1;
            if ($this->access_model->isAdmin($userID)){
                $data["admin"] = 1;
            }
        }
            
        $data["failedSubmit"] = null;
        $data["failedCaptcha"] = null;
        $data['errorMessage'] = null;
        $data["siteTitle"] = $this->config->item("siteTitle");
//                var_dump($data["siteTitle"]);
        if ($this->input->get("e")){
            $e = $this->input->get("e");
            if ($e == "NL"){
                $data['errorMessage'] = "Not Logged In";
            }
        }

        if ($this->input->post("new_submission") ){
            $new_submission = array(
                "course" => $this->input->post("course"),
                "companyName" => $this->input->post("companyName"),
                "location" => $this->input->post("location"),
                "pay" => $this->input->post("pay"),
                "skills" => $this->input->post("skills"),
                "experience" => $this->input->post("experience")
            );
//            var_dump(explode("\n",$new_submission["experience"]));

            // If no user is logged in, get ID of the new entry,
            // and take person to the option of logging in and
            // finally associate that id with new submission


            if ( $userID != null){
                $new_submission["userID"] = $userID;
            }
            $captcha = $this->input->post("captcha");
            $sess_captcha = $this->session->userdata("captcha");
            $answer = $sess_captcha["answer"];
            if ($this->session->userdata("captcha") && $captcha == $answer ){
                $new_submission["companySlug"] = $this->toSlug($this->input->post("companyName"));
                $new_submission["courseSlug"] = $this->toSlug($this->input->post("course"));
                $id = $this->attachments_model->new_submission($new_submission);
                if ($userID == null){
                    $this->session->set_userdata("anonymousSubmission", $id);
                    //if person is not logged in 
                    //redirect to login page
                    //and then after that bring them back and submit to database
                    header("Location: /index.php/access/login?e=anonymousSubmission");
                }
                else {
                    $this->session->unset_userdata("anonymousSubmission");
                    $data["errorMessage"] = "Successful submission. You can fill another one.";
                }
            }
            else{
                $data["failedCaptcha"] = 1;
                $data["failedSubmit"] = $new_submission;
            }
        }   
        if ( $this->session->userdata("anonymousSubmission") && $this->session->userdata("userID") ){
            $id = $this->session->userdata("anonymousSubmission");
            $userID = $this->session->userdata("userID");
            $this->attachments_model->setUserIDForSubmission($id, $userID);
            $this->session->unset_userdata("anonymousSubmission");
        }

        $data["failedContact"] = null;
        if ( $this->input->post("contactSubmit") ){
            // if submitted answer matches text, then continue
            // otherwise post message saying invalid captcha and return
            // the data posted
            $captcha = $this->input->post("captcha");
            $newContact = array(
                "email" => $this->input->post("contactemail"),
                "subject" => $this->input->post("contactsubject"),
                "message" => $this->input->post("contactmessage")
                );
            $sess_captcha = $this->session->userdata("captcha");
            $answer = $sess_captcha["answer"];
            if ($this->session->userdata("captcha") && $captcha == $answer ){
                $this->attachments_model->save_contact($newContact);
                $data["errorMessage"] = "Successful submission. We'll get back to you.";
            }
            else{
                $data["failedCaptcha"] = 1;
                $data["failedContact"] = $newContact;
            }

        }

        $data["advertCompanies"] = $this->advert_model->getHomePageAds();
        $data["topRatedCompanies"] = $this->attachments_model->getTopRatedCompanies();
//                var_dump($data["advertCompanies"]);
        $n = array("answer" => 0);
        while ( $n["answer"] == 0){
            $n = $this->newCaptcha();
        }
        $data["captcha"] = $n;
        $this->session->set_userdata("captcha", $data["captcha"] );
        $this->load->view("purpleTheme/index",$data);
    }

    public function courses($course=""){
        //sort urls
        $this->load->helper('url');

        // decode these items
        $course = rawurldecode($course);

        $data = array("loggedIN" => 0);
        if ($this->session->userdata("userID")){
            $data["loggedIN"] = 1;
        }
        $data["about"]="about";
        $data["courses_link_active"]="active";
        $data["course"]=$course;
        $data["company_name"]= "";
        $data["siteTitle"] = $this->config->item("siteTitle");

        $userID = $this->session->userdata("userID");

        if ($course!=""){
                $c =  $this->attachments_model->get_companies($course);

                if (!$c){
                        show_error("The data requested does not exist");
                }
                $data["companies"] = array();
                foreach ($c as $c1){
                    $c1["link"] = "item/".$c1["SubmissionID"];
                    array_push($data["companies"], $c1);
                }

//			//for the modal that shows up when one is viewing limited no of companies, 5, you should only see it once
//			if (!$this->session->userdata("limited_company_alert_popup") ){
//                            $this->session->set_userdata("limited_company_alert_popup", true);
//                        }
//                        else{
//                            $this->session->set_userdata("hide_popup", true);
//                        }
//
//			//if person is not logged in, we randomly sort the elements to generate, then only give the first five.
//			if (!$this->session->userdata("useremail")){
//				shuffle($data["companies"]);
//				$output = array_slice($data["companies"], 0,5);
//				$data["companies"] = $output;
//			}
//			else if ( $this->session->userdata("priviledge") < 2 || $this->input->get("s")!="p" ){
//				sort($data["companies"]);
//			}
                $this->load->view("purpleTheme/companies",$data);
        }
        else{
                    $c = $this->attachments_model->get_courses();
//			if (!$data["courses"]){
//				show_error("The data requested does not exist");
//			}
                    $data["courses"] = array();
                    foreach ($c as $c1){
                        $c1["link"] = "courses/".$c1["courseSlug"];
                        array_push($data["courses"], $c1);
                    }
                    $this->load->view("purpleTheme/courses",$data);
            }

    }

    public function companies($companySlug = ""){
        //sort urls
        $this->load->helper('url');
        $userID = $this->session->userdata("userID");
        $data = array("loggedIN" => 0);
        if ($this->session->userdata("userID")){
            $data["loggedIN"] = 1;
        }
        $data["siteTitle"] = $this->config->item("siteTitle");

        if ($companySlug!=""){
                $c = $this->attachments_model->get_courses($companySlug);
                if (sizeof($c) == 1){
                        header("Location: /index.php/attachments/item/".$c[0]["SubmissionID"]);
                }
//			if (!$data["courses"]){
//				show_error("The data requested does not exist");
//			}
                $data["courses"] = array();
                foreach ($c as $c1){
                    $c1["link"] = "item/".$c1["SubmissionID"];
                    array_push($data["courses"], $c1);
                }
                $this->load->view("purpleTheme/courses",$data);
        }
        else{
                    $c = $this->attachments_model->get_companies();
                    if (!$c){
                            show_error("The data requested does not exist");
                    }

                    $data["companies"] = array();
                    foreach ($c as $c1){
                        $c1["link"] = "companies/".$c1["companySlug"];
                        array_push($data["companies"], $c1);
                    }
                    //for the modal that shows up when one is viewing limited no of companies, 5, you should only see it once
                    if (!$this->session->userdata("limited_company_alert_popup") ){
                        $this->session->set_userdata("limited_company_alert_popup", true);
                    } 
                    else{
                    $this->session->set_userdata("hide_popup", true);
                     }

                    //if person is not logged in, we randomly sort the elements to generate, then only give the first five.
                    if (!$this->session->userdata("useremail")){
                            shuffle($data["companies"]);
//                            $output = array_slice($data["companies"], 0,5);
//                            $data["companies"] = $output;
                    }
                    else if ( $this->session->userdata("priviledge") < 2 || $this->input->get("s")!="p" ){
                            sort($data["companies"]);
                    }
                    $this->load->view("purpleTheme/companies",$data);
            }


    }

    public function item($submissionID){
        $data = array("loggedIN" => 0);
        if ($this->session->userdata("userID")){
            $data["loggedIN"] = 1;
        }
        if ($submissionID == ""){
            header("Location: /index.php/attachments/courses");
        }
        $data["item"] = $this->attachments_model->getDataForSubmissionID($submissionID);
        $data["siteTitle"] = $this->config->item("siteTitle");
        $courseSlug = $data["item"][0]["courseSlug"];
        $companySlug = $data["item"][0]["companySlug"];
        $experiences = $this->attachments_model->getExperience($courseSlug, $companySlug);
        $submissions = array();
        foreach ($experiences as $exp){
            $temp = array();
            $temp["skills"] = $exp["skills"];
            $temp["experiences"] = $exp["experience"];
            array_push($submissions, $temp);
        }
        $data["submissions"] = $submissions;
        $this->load->view("purpleTheme/courseItem",$data);
    }
} 

?>