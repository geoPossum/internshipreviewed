<?php
class MyData extends CI_Controller{
    public function __construct()
    {
            parent::__construct();
            $this->load->model('mydata_model');
            $this->load->library('session');
            $this->load->library('MyValues');
            $this->config->load("myvalues");
    }
    public function index(){
        $this->status();   
    }
    public function status(){
        $this->load->helper('url');
        $data = array("loggedIN" => 0);
        if ($this->session->userdata("userID")){
            $data["loggedIN"] = 1;
        }
        $data["siteTitle"] = $this->config->item("siteTitle");
//            $this->session->set_userdata("userID", 1);
        $userID = $this->session->userdata("userID");
        if (!$userID){
            redirect("/?e=NL");
        }
        $data["editMenuItems"] = $this->mydata_model->getCompanyCourseSubIDForUser($userID);
        $this->load->view("purpleTheme/editItem",$data);   
    }
    public function getData($submissionID){
        $userID = $this->session->userdata("userID");
        $data = $this->mydata_model->getDataforSubmissionID($userID, $submissionID);
        echo json_encode($data);
    }
    public function updateData(){
        $string = $this->input->post("data");
        $data = array();
        foreach(explode("&", $string) as $chunk){
            $param = explode("=", $chunk);
            $data[urldecode($param[0])] = urldecode($param[1]);
        }
        $this->mydata_model->updateDataForMd5($data);
        var_dump($data);
    }
}