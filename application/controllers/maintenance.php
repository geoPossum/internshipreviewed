<?php
class maintenance extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('maintenance_model');
		$this->load->model('access_model');
		$this->load->model('attachments_model');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->library('MyValues');
                $this->config->load("myvalues");
                $this->load->helper(array('form', 'url'));
	}
	
        public function index($id=""){
		//show interface
                $data["loggedIN"] = 0;
                $data["message"] = "";
                if ($this->session->userdata("userID")){
                    $data["loggedIN"] = 1;
                }
                
		if (!$this->session->userdata("admin")){
//			header("Location: /index.php");
		}
                
                if ($this->input->get("upload")== "success"){
                    $data["message"] = "Successful Upload";
                }
                else if ($this->input->get("upload")== "fail"){
                    $data["message"] = "Upload Failed";
                }
                
		//get number of Survey items that have not been validated
		$data["unvalidatedItems"] = $this->maintenance_model->get_unseen();
		$data["unvalidatedBadge"] = count( $data["unvalidatedItems"] );
		//get number of contacts that have not been seen
		$data["contactedItems"] = $this->maintenance_model->get_contacted();
		$data["contactedBadge"] = count( $data["contactedItems"] );

		$data["courseTabCourses"] = $this->attachments_model->get_courses();
		$data["companyTabCompanies"] = $this->attachments_model->get_companies();
		sort( $data["companyTabCompanies"] );
		sort( $data["courseTabCourses"] );

		$data["title"] = "Maintenance - Launchform";
                $data["siteTitle"] = $this->config->item("siteTitle");
                
		$this->load->view("purpleTheme/maintenanceIndex",$data);
	}

	public function slugs(){
		if (!$this->session->userdata("admin")){
			header("Location: /index.php");
		}
		$data["title"] = "Maintenance - Launchform";

		$data["result"] = $this->maintenance_model->createSlugs();

		$this->load->view("templates/header",$data);
		$this->load->view("templates/nav_menu",$data);
		$this->load->view("templates/mini_header_1",$data);
		$this->load->view("maintenance/regenerate_slugs",$data);
		$this->load->view("templates/footer",$data);
	}

	public function script(){
		$answer="";

		if ( $this->input->post("query") ){
			$post = $this->input->post();
			$result = $this->maintenance_model->run_script($post['query']);

			if ($result){
				$answer .= "<div class='container'>";
				foreach ($result as $item){
					$answer .= "";
					foreach ($item as $key => $value){
						if ($key && $value){
							$answer .= "
							<div class='row'>
								<div class='col-md-5'>
									<strong>".$key."</strong>
								</div>
								<div class='col-md-7'>".
									$value.
								"</div>
							</div>";
						}
					}
						
					$answer .= "<hr class='col-xs-12'>";
				}
				$answer .= "</div>";
			}
			sleep(2);
			echo $answer;
		}
	}
	
        public function unvalidated_company($entry_id=""){

		// show new data
		$data["array_redo"] = $this->myvalues->redo;
		$data["array_company_size"] = $this->myvalues->company_size;
		$data["array_days_of_working"] = $this->myvalues->days_of_working;
		$data["array_dress_code"] = $this->myvalues->dress_code;
		$data["array_hands_on"] = $this->myvalues->hands_on;
		$data["array_interesting"] = $this->myvalues->interesting;


		if (!$this->session->userdata("admin")){
			header("Location: /index.php");
		}
		$data["title"] = "Maintenance - Launchform";

		if ( $this->input->post("validate_this") ){
			$temp_data = $this->input->post();
			if ( $this->input->post("company_name") ){
				$temp_data["company_slug"] = url_title( $this->input->post("company_name") );
			}
			$company_slug = $temp_data["company_slug"];
			$data["temp_data"] = $temp_data;
			//remove input button value and hidden field, not to be inserted into table
			unset( $temp_data["submit_mydata"]);
			unset( $temp_data["validate_this"]);

			$this->access_model->update( $this->input->post("company_slug"), $this->input->post("id"), $this->input->post("email"), $temp_data);
			$this->maintenance_model->validate( $this->input->post("id") );
			$data["finished_validating"] = 1;
			header("Location: /index.php/maintenance");
		}

		$data["user_data"] = $this->maintenance_model->get_unseen_user_data($entry_id);
		$this->load->view("templates/header",$data);
		$this->load->view("templates/nav_menu",$data);
		$this->load->view("templates/mini_header_1",$data);
		$this->load->view("maintenance/edit_entry",$data);
		$this->load->view("templates/footer",$data);
	}
	
        public function update_company_name(){
		$former_name = $this->input->post("former");
		$new_name = $this->input->post("newer");
		$this->maintenance_model->update_company_name($former_name, $new_name);
		$this->maintenance_model->createSlugs();
	}
	
        public function updateCourseName(){
                $data = $this->input->post("x1");
                $x = explode("&", $data);
                $d = array();
                foreach($x as $x1){
//                    var_dump($x1);
                    $w = explode("=", $x1);
                    $d[$w[0]] =$w[1];
                }
		$former_name = urldecode($d["oldName"]);
		$new_name = urldecode($d["newName"]);
		$this->maintenance_model->updateCourseName($former_name, $new_name);
		$this->maintenance_model->createSlugs();
                $ret = $this->maintenance_model->getSlugForCourseName($new_name);
                echo json_encode($ret);
	}
        
        public function updateCompanyName(){
                $data = $this->input->post("x1");
                $x = explode("&", $data);
                $d = array();
                foreach($x as $x1){
//                    var_dump($x1);
                    $w = explode("=", $x1);
                    $d[$w[0]] =$w[1];
                }
		$former_name = urldecode($d["oldName"]);
		$new_name = urldecode($d["newName"]);
		$this->maintenance_model->updateCompanyName($former_name, $new_name);
		$this->maintenance_model->createSlugs();
                $ret = $this->maintenance_model->getSlugForCompanyName($new_name);
                echo json_encode($ret);
	}
	
        public function generate_slugs(){
		$slugs = $this->maintenance_model->createSlugs();
		$data=array("result"=>$slugs);
		$this->load->view("maintenance/regenerate_slugs",$data);
	}
	
        public function editUserdata($id){
            $data["siteTitle"] = "Maintenance - Edit User Data";
            $data["loggedIN"] = 0;
            
            $userID = $this->session->userdata("userID");
            if ($userID){
                $data["loggedIN"] = 1;
                if ($this->access_model->isAdmin($userID)){
                    $data["admin"] = 1;
                }
            }
            
            $data["siteTitle"] = $this->config->item("siteTitle");
            $data["status"] = "";

            if ( $this->input->post("editSubmission") ){
                    $postData =  $this->input->post();
                    unset($postData["editSubmission"]);
                    $SubmissionID = $postData["SubmissionID"];
                    unset($postData["SubmissionID"]);
                    $userID = $postData["userID"];
                    unset($postData["userID"]);
                    $postData["seen"] = 1;
                    $x = $this->maintenance_model->updateSurveyDataWithIDs( $SubmissionID, $postData);
                    var_dump($x);
                    $data["status"] = "SuccessFull Edit!";
            }

            $x = $this->maintenance_model->getSurveyDataForID($id);
            $data["userData"] = $x[0];
            $this->load->view("purpleTheme/maintenanceEditItem",$data);
	}
	
        public function delete($id){
		$this->maintenance_model->delete_entry($id);
		header("Location: /index.php/".$this->session->userdata("previous_viewed_url") );
	}
	
        private function field_helper($template, $array){
		/********************************
		Adds each item in array into the survey array as passed
		then finally sets survey in session
		does:
		survey[$item] = post[item]
		 *******************************/
		if (!is_array( $array ) ){
			return;
		}

		$output = array();
		foreach ($template as $key){ 
			$new_key = str_replace("survey_", "", $key);

			if ( isset($array[$key]) ){
				$output[$new_key] = $array[$key];
			}
			else{
				$output[$new_key] = "";
			}
			
		}
		return $output;
	}

	private function send_email($email, $token){
		$this->load->library('email');
		$this->load->library('parser');

		$data = array(
            'email' => urlencode($email),
            'token' => urlencode($token)
            );

		$this->email->from('info@launchform.com', 'Launchform');
		$this->email->to($email);

		$message = $this->parser->parse('maintenance/email_body_template', $data, TRUE);

		$this->email->subject('Welcome to Launchform');
		$this->email->message( $message );

		$this->email->send();
		
		// echo $message; 
	}

	public function survey_data(){
		//d
		// echo 1;

		$template= array(
			"survey_email",
			"survey_course",
			"survey_calendar_year_attached",
			"survey_academic_year_attached",
			"survey_company_name",
			"survey_company_location",
			"survey_company_size",
			"survey_maximum_duration",
			"survey_working_days",
			"survey_pay",
			"survey_dress_code",
			"survey_nature_of_work",
			"survey_skills",
			"survey_testimony",
			"survey_hands_on",
			"survey_interesting",
			"survey_innovation",
			"survey_redo"
			);
		$post = $this->input->post();
		$input = explode("&", $post["data"]);
		$survey = array();
		foreach ($input as $key => $value) {
			$field = explode("=", $value);
			$survey[urldecode($field[0])] = urldecode($field[1]);
		}
		$survey = $this->field_helper($template, $survey);


		$survey["course_slug"] = strtolower( url_title( $survey["course"]) ); 
		$survey["company_slug"] = strtolower( url_title( $survey["company_name"]) );
		$data["debug"] = $this->maintenance_model->insert_survey_data( $survey );

		if ( $survey["email"] ){
				$email = $survey["email"];
				//get email insert that email into myusers
				$this->access_model->new_user( $survey["email"] );
				$token = $this->access_model->get_token_for_email($survey["email"]);
				$this->send_email($survey["email"], $token);
		}

		echo "Done";
	}
        
        public function uploadFile(){
            $config['upload_path'] = './upload/';
            $config['allowed_types'] = 'text|txt';
            $config['max_size']	= '100';
            $config['overwrite']  = True;
            $this->load->library('upload', $config);
            $error = $d = "";
            
            if ( ! $this->upload->do_upload()){
                $error = array('error' => $this->upload->display_errors());
                $data["error"] = $error;
                header("Location: /index.php/maintenance?upload=fail");
            }
            else{
                $d = array('upload_data' => $this->upload->data());
                $path = $d["upload_data"]["full_path"];
                $uploads = maintenance::readCSV($path);
                $this->maintenance_model->insertSurveyBatch($uploads);
                $data["error"] = $uploads;
                header("Location: /index.php/maintenance?upload=success");
            }
            
        }
        
        private function readCSV($filename){
            $row = 0;
            $batch = array();
            if (($handle = fopen($filename, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row != 0){
                        $upload = array(
                            "email" => $data[0],
                            "course" => $data[1],
                            "companyName" => $data[2],
                            "location" => $data[3],
                            "pay" => $data[4],
                            "skills" => $data[5],
                            "experience" => $data[6],
                        );
                        array_push($batch, $upload);
                    }
                    $row++;
                }
                fclose($handle);
            }
            return $batch;
        }
        
        public function validateAll(){
            $this->maintenance_model->validateAll();
            header("Location: /index.php/maintenance/");
        }
        
        public function markContactRead(){
            $id = $this->input->get("ID");
            $this->maintenance_model->markContactRead($id);
            echo "Done";
        }
}
?>