<?php
class Access extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('access_model');
		$this->config->load("myvalues");
		$this->load->helper('url');
		$this->load->library('google');
		$this->load->library('session');
		$this->load->library('email');
		$this->load->library('MyValues');
		$this->load->library('Errors');
	}

	public function login( $message=""){
                
		$data = array(
                    "loggedIN" => 0,
                    "errorMessage" => null
                    );
                if ($this->session->userdata("userData")){
                    $data["loggedIN"] = 1;
                }
                if ($this->input->get("e")){
                    $e = $this->input->get("e");
                    $message = null;
                    if ($e == "anonymousSubmission"){
                        $message = "Submission Complete!<br/> Please login to be able to edit your submission later.";
                    }
                    else if ($e == "authUsed" ){
                        $message = "There was a problem with login.<br/> Please try again. ";
                    }
                    else{
                        $message = $e;
                    }
                    
                }
                $data["siteTitle"] = $this->config->item("siteTitle");
                
                $data["facebookURL"] = $this->config->item("facebookURL");
                $data["errorMessage"] = $message;
                
		$this->load->view("purpleTheme/login",$data);
	}

	public function googlecallback( $state = "" ){
		$data = array(
			"debug" => "",
			"title"=> "Login");

		if ( $this->session->userdata("useremail") ){
			// header("Location:".$this->session->userdata('previous_viewed_url'));
			if ( $this->input->get("state") == "survey" || $state == "survey" ){
				// header("Location:".$this->session->userdata('previous_viewed_url'));
				header("Location:/index.php/survey");
			}
			else{
		  		header("Location:/index.php/");
		  	}
		}

		$client_id = $this->config->item("google_client_id");
		$client_secret = $this->config->item("google_client_secret");
		$redirect_uri = $this->config->item("google_redirect_uri");
		$response_type = $this->config->item("google_response_type");

		$client = new Google_Client();
		$client->setApplicationName("Launchform");
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirect_uri);
		$client->setScopes(array('https://www.googleapis.com/auth/userinfo.email','https://www.googleapis.com/auth/userinfo.profile'));
		$client->setState($state);

		/************************************************
		  When we create the service here, we pass the
		  client to it. The client then queries the service
		  for the required scopes, and uses that when
		  generating the authentication URL later.
		 ************************************************/
		$service = new Google_Service_Oauth2($client);

		if ($this->input->get("error")){
			//there was a problem validating
			header("Location:/index.php/access/login/?message=gerror");
			return;
		}


		/************************************************
		  If we have a code back from the OAuth 2.0 flow,
		  we need to exchange that with the authenticate()
		  function. We store the resultant access token
		  bundle in the session, and redirect to ourself.
		 ************************************************/
		if ( $this->input->get("code")){
			//exchange auth code for token
			$code = $this->input->get("code");
			$client->authenticate( $code );
			$access_token = $client->getAccessToken();
			$this->session->set_userdata("access_token", $access_token);
			if ( $this->input->get("state") == "survey" || $state == "survey" ){
				header("Location:$redirect_uri/survey");
			}
			else{
				header("Location:$redirect_uri");
			}
			return;
		}

		/************************************************
		  If we have an access token, we can make
		  requests, else we generate an authentication URL.
		 ************************************************/
		if ( $this->session->userdata("access_token") ){
			$client->setAccessToken( $this->session->userdata("access_token") );
		}
		else{
			$loc = $client->createAuthUrl();
			header("Location: $loc");
		}

		/************************************************
		  If we're signed then we get the email and check
		 ************************************************/
		$email = $big = $x1 = 'UNSET';
		
		if ( $client->getAccessToken() ){
		  	$email = $service->userinfo->get(array("email"))->getemail();
		  	$error_message = $this->check_user($email, $state);
		}

		$data["error_message"] = $error_message;
		$data["error_message_type"] = "danger";

		$this->load->view("templates/header",$data);
		$this->load->view("templates/nav_menu",$data);
		$this->load->view("access/login",$data);
		$this->load->view("templates/footer",$data);
	}

	public function facebookcallback( $state = ""){
		$code = $this->input->get("code");
                $error = $this->input->get("error");
                if ($code){
                    $response = json_decode(file_get_contents( $this->config->item("appAccessToken") ));
                    $app_access_token = $response->access_token;
                    $response2 = json_decode(file_get_contents( $this->config->item("accessTokenURL").$code ));
                    //var_dump($response);
                    //echo "<br/>";
                    //var_dump($response2);
                    //echo "<br/>";
                    if ( array_key_exists("error", get_object_vars($response2))){
//                        echo $response2->error->message;
                        //take to login page with click button again.
                        header("Location: /index.php/access/login/?e=authUsed");
                    }
                    else{
                        $access_token_2 = $response2->access_token;
                        $url = $this->config->item("validateTokenURL").$access_token_2.
                            "&access_token=".$app_access_token;
                        $response3 = json_decode(file_get_contents($url));
                        $valid = $response3->data->is_valid;
                        if ($valid){
                                $this->getFbUserData($access_token_2);
                        }
                    }
                }
                if ($error){
                    //access has been denied
                }
                end:
                    die();
	}
        
        private function getFbUserData($access_token_2){
            $access_token = $access_token_2;
            $this->session->set_userdata("fbAccessToken", $access_token );
            $url_4 = $this->config->item("fBMeEndpoint").$access_token;
            $response = json_decode(file_get_contents($url_4));
//            var_dump($response);
            if (
                    array_key_exists("id", get_object_vars($response)) && 
                    array_key_exists("name", get_object_vars($response))
               ){
                
                $fbid = $response->id;
                $name = $response->name;
                $d = array(
                    "fbId" => $fbid,
                    "name" => $name,
                    "email" => ""
                );
                if (array_key_exists("email", get_object_vars($response))){
                    $d["email"] = $response->email;
                }
//                var_dump($d);
                $id2 = $this->access_model->insertFBUpdateUser($d);
//                var_dump($id2);
                $this->session->set_userdata("userID", $id2);
                header("Location: /index.php");
            }
            else{
                echo "Error";
            }
        }

	public function logout(){
		//destroy entire session , not so effective, change this later.
		$this->session->sess_destroy();
		header("Location: /index.php/");
	}

}	