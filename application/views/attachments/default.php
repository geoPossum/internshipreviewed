<!-- header and mini -->
	

		<div class="landing-page jumbo">
		  <div class="row">
		    <h2 class="text col-xs-10 col-xs-offset-1">Welcome to Launchform!</h2>
		    <div class="bg col-xs-4 col-xs-offset-4"></div>
		  </div>
		  <div class="row">
		    <h4 class='text col-xs-6 col-xs-offset-3'>You are just about to find a cool attachment!<br><br>Simply submit data and gain full access</h4>
		    <div class="bg col-xs-4 col-xs-offset-4"></div>
		  </div>
		</div>

	<div class="landpage">
		<?php
			if ( isset( $error) ){
				echo "<div class=\"row\">
				<h3 class=\"col-xs-4 col-xs-offset-4\"><span class=\"label label-danger\"> $error </span></h3>
				</div>";
			}
		?>
	</div>


	<section class="landing-page row basic-navigation">
		<div class="container">

			<?php if ( isset($contact_messsage_sent) ){ ?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
			      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			      <?php echo $contact_messsage_sent ?>
			    </div>
		    <?php } ?>
			<h2 class='text-center'>Basic Navigation</h2>

			<hr class='visible-xs'>	
			<div class="col-sm-4">
                            <?php echo $cap["image"];?>
				<div class="item">
					<div class="topic">Courses</div>
					<!-- <div class='img'><img class="img-responsive" src="/bin/images/graduation2.jpeg"></div> -->
					<div class='text'>Check Attachments according to course</div>
					<div class="visit"><a class="btn btn-brown" href="/index.php/attachments/courses/">Visit</a></div>
				</div>
			</div>
				
			<hr class='visible-xs'>	
			<div class="col-sm-4">
				<div class="item">
					<div class="topic">Upload an experience</div>
					<!-- <div class='img'><img class=""  src="/bin/images/survey.png"></div> -->
					<div class='text'>Contribute info anonymously so everyone gets more info</div>
					<div class="visit"><a class="btn btn-brown" href="/index.php/survey">Visit</a></div>
				</div>
			</div>

			<hr class='visible-xs'>
			<div class="col-sm-4">
				<div class=" item">
					<div class="topic">Companies</div>
					<!-- <div class='img'><img class=" img-responsive" 	src="/bin/images/industry2.png"></div> -->
					<div class='text'>Live the attachment experience at various companies listed</div>
					<div class="visit"><a class="btn btn-brown" href="/index.php/attachments/companies/">Visit</a></div>
				</div>
			</div>
		</div>
	</section>

	<section class="landing-page row privacy" id="privacy">
		<div class="container">
			<h1 class='text-center'>Privacy</h1>

			<div class="jumbotron">
				<h2 class="privacy-safe">You <sub>and your data</sub> are safe!</h2>
				<p class="lead">
					The data that is submitted is treated with the utmost of privacy and presented anonymously.
				</p>
			</div>

			<div class="privacy-statement" id="privacy-belief">
				We believe that if a person believes that their anonymity is ensured in any process, they would "speak" freely and honestly about attachments and experiences they have had at different companies without fear of reproof or reprisals. Any data submitted is presented anonymously on the site. The data that we ask you to submit that seems personal in nature is solely for the purpose of being able to present more meaningful data that may help others e.g. course, year, if paid etc. The data is not linkable to you.
			</div>

			<div class="privacy-statement" id="privacy-email">
				The email adress we require of you is solely for the purpose of enabling you to have an account. It is <strong>NOT MANDATORY</strong> but highly recommended to enable you <strong><em>to edit any posts you submit</em></strong> later. We shall be moving to inco-orporate Facebook and Google Logins later.
			</div>
			<div class="privacy-statement">

				Lastly, keep sharing information on the site.
				<blockquote class="blockquote-reverse">
					<p>It is better to give than to receive.</p>
					<footer>Jesus Christ</footer>
				</blockquote>
			</div>

			<hr class='visible-xs'>	
			<div class="row">
				<div class="col-md-4 col-md-offset-4">

				</div>
			</div>
		</div>
	</section>

	<section class="landing-page row contact-us" id="contact">
		<div class="container">
			<h2 class='text-center'>Contact Us</h2>

			<div class="row">
				<?php
				if ( isset($message) ){
					echo "<h3> <span class=\"label label-success\">$message</span> </h3>";
				}
				?>
				<form class="form-horizontal" role="form" method="POST" action=<?php echo '"'.$_SERVER["PHP_SELF"].'"';?> >
					
                                        <div class="form-group">
						<div class="col-sm-2">
						    <label for="email">Email address</label>
						</div>
					    <div class="col-sm-10">
						    <input type="email" class="form-control" name="contact_email" id="email" placeholder="x@y.com">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2">
						    <label for="subject">Subject</label>
					    </div>
					    <div class="col-sm-10">
						    <input type="text" class="form-control" name="contact_subject" id="subject" placeholder="This is concerning...">
					    </div>
					</div>
					<div class="form-group">
						<div class="col-sm-2">
							<label for="exampleInputPassword1">Body</label>
						</div>
						<div class="col-sm-10">
							<textarea class="form-control" rows="5" name="contact_body"></textarea>
						</div>
					</div>
					<div class="form-group">
						<input type="submit" name="contact_submit" class="btn btn-primary" value="Submit"/>
					</div>
				</form>
			</div>
		</div>
	</section>

	
	<div class="container">