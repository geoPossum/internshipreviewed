<?php if ( $this->session->userdata('admin') ){
	// var_dump($user_data);
	$id = $user_data[0]["id"];
	?>
	<style type="text/css">
	#admin-bar{
		box-sizing:box-model;
		/*left: 1em;*/
		padding: 0.5em 1em;
		position: fixed;
		top: calc(50% - 1.5em);
		/*height: 3em;*/
		background-color: #B7521E;
		/*border-radius: 3px;*/
		color: white;
	}
	#admin-bar a{
		color: white;
	}
	#admin-bar a:hover{
		text-decoration: none;
	}
	#admin-bar:hover{
		background-color: #983A09;
	}
	</style>
	<div id='admin-bar'>
		<a href="/index.php/maintenance/edit_userdata/<?php echo $id;?>">Edit<br>This <br>Data</a>
	</div>
	<?php
}
?>

<div class="container">
<?php 
// foreach ($user_data as $key => $item) {
// 	var_dump($item);
// 	echo "$key<br>";	
// }
foreach ( array("hands_on_value","interesting_value","redo_value") as $item) {
	if ( isset($user_data[$item]) ){
		if ( $user_data[$item] == '0' ){
			$user_data[$item] = 'N/A';
		}

		if ($item=="hands_on_value"){
			$hands_on_value = $user_data[$item];
		}
		else if ($item=="interesting_value"){
			$interesting_value = $user_data[$item];
		}
		else if ($item=="redo_value"){
			if ($user_data[$item] =='50'){
				$user_data[$item] = '0';
			}
			$redo_value = $user_data[$item];
		}
		unset($user_data[$item]);
	}
}
// foreach ($user_data as $item) {
// 	var_dump($item);
// 	echo "<br>";	
// }

$company_info = $user_data[0];
$course_name = ucwords($company_info["course"]);
echo<<<EOD
<div class='row text-center userdata-course-name'>
	<div class="col-md-offset-3 col-md-9">
		<h3> <span class='label label-brown'> $course_name </span> </h3>
	</div>
</div>
EOD;


$company_info["company_name"] = ucwords($company_info["company_name"]);
echo<<<EOD
<div class="row">
	<div class="col-md-3">
		<div id="summary">
			<div id="company">
				<div class="name">   $company_info[company_name]</div>
				<div class="nature"> $company_info[nature_of_work]</div>
				<div class="pay"> Pay:$company_info[pay]</div>
				<div class="location">$company_info[company_location]</div>
			</div>

			<div id="user">
				<div class="summary">Summary</div>
				<div class="progress progress-striped">
					<div class="progress-bar " id="hands-on" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						<span>Hands-On: $hands_on_value% </span>
				  	</div>
				</div>
				<div class="progress progress-striped">
			  		<div class="progress-bar progress-bar-success" id="interesting" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
			    		<span>Interesting: $interesting_value%</span>
			 	 	</div>
				</div>
				<div class="progress progress-striped">
			  		<div class="progress-bar progress-bar-warning" id="recommendation" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
			    		<span>Do it again: $redo_value% </span>
			  		</div>
				</div>
			</div>
		</div>
	</div>
EOD;
echo "<div id=\"entries\" class=\"col-md-9\">";
	$count = 1;
	foreach ($user_data as $entry_info) {
		echo "
		<div class='entry col-xs-12'>
			<div class=\"title\">Entry #$count</div>";
			$count +=1;
			foreach ( array("skills","testimony","innovation") as $key) {
				//modify redo so that it does not print out 1, but instead a value
				if (!$entry_info[$key]){
					continue;
				}
					echo "
				<div class=\"col-xs-12 item $key\">
					<div class=\"field_name col-md-5 $key\">".str_replace("_"," ",ucwords($key)).":</div>
					<div class=\"col-md-7\">$entry_info[$key]</div>
				</div>";
			}
			$redo_map = array('1.00' => "Yes", '1' => "Yes", '0.5' => 'No' );
			$redo_value = settype($entry_info["redo"], "integer");
			echo <<<EOD
			<div class="col-xs-12 item redo">
				<div class="field_name col-md-5">Recommend the experience to someone in a similar situation?:</div>
				<div class="col-md-7">$redo_map[$redo_value]</div>
			</div>
EOD;

			// calendar_year_attached,academic_year_attached,company_size,
			// maximum_duration,working_days,pay,dress_code
		
		if ($entry_info["working_days"]){
			$entry_info["working_days"] = $this->myvalues->days_of_working[$entry_info["working_days"]*count($this->myvalues->days_of_working)-1];
		}
		if ($entry_info["dress_code"]){
			$entry_info["dress_code"] = $this->myvalues->dress_code[$entry_info["dress_code"]*count($this->myvalues->dress_code)-1];
		}
		
		echo <<<EOD
			<div id='hidden_zone_$count' class="collapse" >
				<div class="col-xs-12">
				<hr class="col-xs-6 col-xs-offset-3">
				</div>
EOD;
				foreach ( array("calendar_year_attached","academic_year_attached","maximum_duration", "working_days", "dress_code") as $key) {
					$upper_key = str_replace("_", " ",  ucwords($key));
					if ($key=="maximum_duration"){
						$upper_key = "Default Duration of Attachment offered";
					}
					echo <<<EOD
			<div class="col-xs-12 item $key">
				<div class="field_name col-md-5 $key">$upper_key</div>
				<div class="col-md-7">$entry_info[$key]</div>
			</div>
EOD;
				}
		echo <<<EOD
			</div>
			<div class="row text-center">
				<a class="btn btn-brown" href="#hidden_zone_$count" data-target="#hidden_zone_$count" data-toggle="collapse" >Show/Hide More</a>
			</div>
		</div>
		<hr class="col-xs-12">
EOD;
	}
	echo "</div>";
?>
</div>
