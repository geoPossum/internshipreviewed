<div class="container">
	<?php
	
	if ( empty($companies) ){
		echo "No Company Data";
		return;
	}
	if ( !$this->session->userdata("useremail") ){
		echo "
		<div>
			<h4><span class=\"label label-warning\"><a href=\"/index.php/access/login\">Submit Data</a> to see more data and to access more functionality such as <span style=\"color:black;\">sort by pay</span>, highlight <span style=\"color:black;\">high value companies</span> etc.</span></h4>
		</div>";
		echo $this->session->userdata("priviledge");
	}
	else if ( $this->session->userdata("priviledge")<2 ){
		echo<<<EOD
		<script>
      var page_like_or_unlike_callback = function(url, html_element) {
          $.get(
            "/index.php/access/update_priviledge",
            function(data, status){
              console.log(data+" "+status);
              if ( data=="success"){
                location.reload();
              }
            }
            );
      }

      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1391629301119712',
          xfbml      : true,
          version    : 'v2.1'
        });
        FB.Event.subscribe('edge.create', page_like_or_unlike_callback);


        FB.getLoginStatus(function(response) {
          if (response && response.authResponse) {
            console.log("Logged in");
            var accessToken = response.authResponse.accessToken
            console.log("AccessT "+accessToken);
          }
          else{
            FB.login(function(response){
              // console.log("Login " +response);
              // console.log( "Login " + Object.keys(response));
              // console.log("Login " +Object.keys(response.authResponse));
              var accessToken = response.authResponse.accessToken;
              // console.log("Login Token " +accessToken);
              // console.log("Login " +response.status);
              }, { }
            );
          }
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>


    
  <div id="fb-root"></div>
  <div class="text-center row color-brown" style="margin-bottom:0.5em;">
  <div class="fb-like text-center" data-href="https://www.facebook.com/pages/LaunchForm/1548915102029388" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
  </div>
  <span class=\"label label-warning \" ><strong>Like</strong> the page on facebook and enable sort by pay.<br>
  <small>If you already like, you may have to unlike(hover over the tick) then relike the page to unlock.</small></span>
  </div>
EOD;
	}
	?>
	<table class="table table-condensed table-bordered table-hover">
		<thead>
			<th><a href="<?php echo current_url();?>">
				Companies <span class="glyphicon glyphicon-sort"></span>
				<?php if ( !$this->session->userdata("useremail") ){ echo "<small>Max. 5 Companies</small>"; }?>
				</a> </th>
			<?php
				if ( is_array($companies) ){
					if ( is_array($companies[0]) ){
						if ( isset($companies[0]["pay_slug"]) && $this->session->userdata("priviledge") ){
							echo "<th><a href=\"$_SERVER[PHP_SELF]?s=p\">Pay";
              if ( $this->session->userdata("priviledge") > 1){
                echo "<span class=\"glyphicon glyphicon-sort\"></span>";
              }
              echo "</a></th>";
						}
					}
				}
			?>
		</thead>
		<tbody>
	<?php
	foreach ($companies as $value){
		//highlight different aspects
		//red for less than 3000/=
		//green for 10k plus

		if ( isset($value["pay_slug"] ) && $this->session->userdata("priviledge") 	&& $this->session->userdata("priviledge") > 1 ){
			if ($value["pay_slug"]>=10000){
				echo "<tr class='good-pay'>";
			}
			else if ( $value["pay_slug"]<3000){
				echo "<tr class='bad-pay'>";
			}
			else{
				echo "<tr>";
			}
		}
		else{
			//pay_slug is not set
			echo "<tr>";
		}	

		echo "<td><a href=/index.php/";

		//can be attachments or access
		echo $this->uri->segment(1)."/";

		if ( isset($course) && $course!=''){
			echo "/courses/".rawurlencode($course);
		}
		else{
			echo $this->uri->segment(2);
		}


		echo "/".rawurlencode($value["company_slug"]).">".ucwords($value["company_name"])."</a>";
		echo "</td>";
		// var_dump($value["pay"]);
		if ( isset($value["pay_slug"]) && $this->session->userdata("priviledge") ){
			echo "<td>".$value["pay"]."</td>";
		}
		echo "</tr>";
	}

	?>
		</tbody>
	</table>

<div class="modal " id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">Kindly Login</h4>
              </div>
              <div class="modal-body">
                  <p>You are limited to seeing only 5 companies at a time. You can see new ones by refreshing.  <strong>Submit data</strong> then login to see all companies listed.</p>
                  <small><p>By logging in, people will still not be able to <strong>tie you </strong> to your data. It is still presented <strong>anonymously</strong>.</p></small>
              </div>
              <div class="modal-footer">
                <a class="btn login-custom" href="/index.php/access/login">Login</a>
                <button type="button" class="btn btn-brown" data-dismiss="modal">Okay</button>
          </div>
      </div>
    </div>
  </div>

  <script type='text/javascript'>
  $(document).ready(function(){
    <?php
      if ( !$this->session->userdata("useremail") && !$this->session->userdata("hide_popup") ){
        echo "$('#basicModal').modal();";
      };
    ?>
  });
  </script>