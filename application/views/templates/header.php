<?php
$former_url = $this->session->userdata("current_viewed_url");
$current_url = uri_string();

$segs = explode("/", $current_url);
if ( count($segs) > 2 ){
	$current_url = $segs[0]."/".$segs[1];
}

$segs = explode("/", $former_url);
if ( count($segs) > 2 ){
	$former_url = $segs[0]."/".$segs[1];
}

if ($former_url != $current_url && ( $former_url!="access/forgotpass" || $current_url!="access_resetpass") ){
	$data  =array(
	"previous_viewed_url"=> $former_url,
	"current_viewed_url" => $current_url
	);
	$this->session->set_userdata($data);
}
?>
<!DOCTYPE html>
<head>
	<title><?php echo "Launchform | ".$title ?> </title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="George Nyoro">
	<link rel="shortcut icon" href="/bin/images/favicon.ico">
	<meta name="description" content="Are you looking for an industrial attachment? Click to check out various industrial attachment experiences at companies as submitted by hundreds of students.">
	<meta name="keywords" content="Attachments, Internships, Jobs, Kenya, Student, Industrial Attachment, Industrial Training">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()."bin/bootstrap/dist/css/bootstrap.min.css"; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()."bin/css/theme1.css"; ?>">
	<script type="text/javascript" src="<?php echo base_url()."bin/jquery.js"; ?>"></script>
	<script type="text/javascript" src="<?php echo base_url()."bin/bootstrap/dist/js/bootstrap.js"; ?>"></script>
</head>
<body class="<?php if (isset($landing_page)){echo "body_landing_page";}?>">
	<script type="text/javascript" src="<?php echo base_url()."bin/trackingcode.js"; ?>"></script>