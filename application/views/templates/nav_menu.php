<nav class="navbar navbar-default <?php if(uri_string()=="" || uri_string()=="attachments"){ echo "navbar-fixed-top landing-page";} else{ echo uri_string();} ?>" role="navigation">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand <?php if(isset($index_link_active)){echo $index_link_active;}?>" href="/index.php"> <span class="glyphicon glyphicon-home"></span> Launchform</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="<?php if(isset($courses_link_active)){echo $courses_link_active;}?>"><a href="/index.php/attachments/courses/">Courses <span class="glyphicon glyphicon-eye-open"></span> </a></li>
          <li class="<?php if(isset($companies_link_active)){echo $companies_link_active;}?>"><a href="/index.php/attachments/companies/">Companies <span class="glyphicon glyphicon-hdd"></span></a></li>
          <li><a href="/news" >News/Highlights <span class="glyphicon glyphicon-inbox"></span></a></li>
         <!-- <li><a href="/index.php/info/about">Info <span class="glyphicon glyphicon-info-sign"></span></a></li> -->
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="<?php if(isset($survey_link_active)){echo $survey_link_active;}?>"><a href="/index.php/survey">Survey <span class="glyphicon glyphicon-comment" ></span></a></li>
            
              <?php
              if ( $this->session->userdata("admin") > 4){
                $maintenance = "<li><a href='/index.php/maintenance'>Maintenance <span class='glyphicon glyphicon-cog'></span></a></li>";
              }
              else{
                $maintenance = "";
              }

              if ( $this->session->userdata("useremail")){
                echo<<<EOD
                <li class=dropdown>
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Logged in <span class="glyphicon glyphicon-user"></span><span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    $maintenance
                    <li><a href='/index.php/access/mydata'>My Data <span class='glyphicon glyphicon-edit'></span></a></li>
                    <li class="divider"></li>
                    <li><a href='/index.php/access/logout'>Logout <span class='glyphicon glyphicon-log-out'></span></a></li>
                  </ul>
                </li>
EOD;
           }
              else{
                echo "<li><a href='/index.php/access/login'> Login <span class='glyphicon glyphicon-log-in'></span></a></li>";
              }
            ?>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
