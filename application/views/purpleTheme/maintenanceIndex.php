<!DOCTYPE html>
<html>
<head>
    <title><?php echo $siteTitle; ?> - Maintenance</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="George Nyoro">
    <link rel="shortcut icon" href="/bin/images/favicon.ico">
    <meta name="description" content="Are you looking for an internship? 
        Click to check out various industrial attachment experiences at 
        companies as submitted by hundreds of students.">
    <meta name="keywords" content="Attachments, Internships, Jobs, Kenya, 
        Student, Industrial Attachment, Industrial Training">
    <link rel="stylesheet" type="text/css" href="/bin/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href='/bin/font-awesome-4.1.0/css/font-awesome.css'>
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeOther.css">
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeMaintenance.css">
    <script type="text/javascript" src="/bin/jquery.js"></script>
   
</head>
<body>
    <?php include("navmenu.php");?>
    
    <div id="body">
        <?php if (!empty($message)):?>
        <div class="message"><?php echo $message; ?> </div>
        <?php endif; ?>
        
        <!-- tabs-->
        <div id="tabs">
            <ul class="nav nav-pills" role="tablist">
                <li role="presentation">
                    <a class="link" data-next="newItems-page"  href="#">New Items <span class="badge"><?php echo $unvalidatedBadge; ?></span>
                    </a>
                </li>
                
                <li role="presentation" class="active">
                    <a class="link " data-next="contacts-page" href="#">Contacts <span class="badge"><?php echo $contactedBadge; ?></span> 
                    </a>
                </li>
                
                <li role="presentation" ><a class="link " data-next="courses-page" href="#">Courses</a></li>
                <li role="presentation"><a class="link " data-next="companies-page" href="#">Companies</a></li>
                <li role="presentation"><a class="link " data-next="upload-page" href="#">Upload New Data</a></li>
            </ul>
        </div>
        
        <!--newItems-->
        <div class="page" id="newItems-page">
            <div>
                <table class="table table-striped">
                    <thead>
                        <th>Company Name</th>
                        <th>Course Name</th>
                    </thead>
                    <tbody>
                        <?php foreach($unvalidatedItems as $unvalidatedItem): ?>
                        <tr>
                            <td><a href="/index.php/maintenance/editUserdata/<?php echo $unvalidatedItem["SubmissionID"]?>"><?php echo $unvalidatedItem["companyName"]?><a/></td>
                            <td><a href="/index.php/maintenance/editUserdata/<?php echo $unvalidatedItem["SubmissionID"]?>"><?php echo $unvalidatedItem["course"]?><a/></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                
                <a href="/index.php/maintenance/validateAll"> <div id="validateAll">Validate All</div> </a>
            </div>
        </div>
        
        <!--contacts-->
        <div class="page nohide" id="contacts-page">
            <?php foreach($contactedItems as $item): ?>
            <div class="contactItem">
                <div class="contact-email"><?php echo $item["email"]; ?></div>
                <div class="contact-datetime"><?php echo $item["datetime"]; ?></div>
                <div class="contact-subject"><?php echo $item["subject"]; ?></div>
                <div class="contact-message"><?php echo nl2br($item["message"]); ?></div>
                <a class="contactRef" href="#" data-ref="<?php echo $item["contactID"]; ?>" ><div class="contact">Mark Read</div></a>
            </div>
            <?php endforeach; ?>
        </div>
        
        <!--courses-->
        <div class="page" id="courses-page">
            <?php // var_dump($courseTabCourses); ?>
            <table class="table table-striped">
                <thead>
                <th> Course Name </th>
                <th> Slug </th>
                <th> <i class="fa fa-edit"></i></th>
                </thead>
                <tbody>
                <?php foreach($courseTabCourses as $c):?>
                    <tr>
                        <td class="course"><?php echo $c["course"]; ?></td>
                        <td class="courseSlug"><?php echo $c["courseSlug"]; ?></td>
                        <td> <a class="editCourseItem" href="#"><i class="fa fa-edit"></i></a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        
        <!--companies-->
        <div class="page" id="companies-page">
            <table class="table table-striped">
                <thead>
                <th> Company Name </th>
                <th> Slug </th>
                <th> <i class="fa fa-edit"></i></th>
                </thead>
                <tbody>
                <?php foreach($companyTabCompanies as $c):?>
                    <tr>
                        <td class="company"><?php echo $c["companyName"]; ?></td>
                        <td class="companySlug"><?php echo $c["companySlug"]; ?></td>
                        <td> <a class="editCompanyItem" href="#"><i class="fa fa-edit"></i></a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        
        <!--upload-->
        <div class="page" id="upload-page" >
            
            <form method="POST" action="/index.php/maintenance/uploadFile" id="uploadItems" 
                  enctype="multipart/form-data">
                <div>Choose CSV file to upload: </div>
                <input type="file" name="userfile" >
                <input type="submit" name="submit" value="Upload"> 
            </form>
        </div>
        
    </div>
    
    <div id="modalCourse">
        <div class="inner">
            <form id="modalFormCourse" action="/index.php/maintenance/updateCourseName" method="POST">
                
                <div class="item">
                    <div class="text">Course</div>
                    <input type="hidden" class="course" name="oldName" value="" />
                    <input type="text" class="course" name="newName" value="" />
                </div>
                <div class="item">
                    <input type="submit" value="Update" />
                </div>
                <a id="close" href="#">Close</a>
            </form>
        </div>
    </div>
    
    <div id="modalCompany">
        <div class="inner">
            <form id="modalFormCompany" action="/index.php/maintenance/updateCompanyName" method="POST">
                
                <div class="item">
                    <div class="text">Company</div>
                    <input type="hidden" class="company" name="oldName" value="" />
                    <input type="text" class="company" name="newName" value="" />
                </div>
                <div class="item">
                    <input type="submit" value="Update" />
                </div>
                <a id="close2" href="#">Close</a>
            </form>
        </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function(){
            var activeEdit;
            $("#modalCourse").css("display","none");
            $("#modalCompany").css("display","none");
            
            $(".link").click(function(event){
                event.preventDefault();
                $(".link").parent("li").removeClass("active");
                var next = $(this).attr("data-next");
                $(".page").css("display", "none");
                $("#"+next).css("display", "block");
                $(this).parent().addClass("active");
            });
            
            $(".editCourseItem").click(function(e){
                e.preventDefault();
                activeEdit = $(this);
                $("#modalCourse").css("display","block");
                var parent = $(this).parent().parent();
                var course = parent.children(".course").text();
                $("#modalFormCourse .course").val(course);
            });
           
            $("#modalFormCourse").submit(function(e){
                e.preventDefault();
                var x = $(this).serialize();
                $("#modalFormCourse input").attr("disabled", "disabled" );
                $.post("/index.php/maintenance/updateCourseName", { x1: x }, function(data, status){
                    console.log(data); 
                    var json= JSON.parse(data);
                    if (status === "success"){
                        $("#modalCourse").css("display","none");
                        var parent = activeEdit.parent().parent();
                        parent.children(".course").text(json["course"]);
                        parent.children(".courseSlug").text(json["courseSlug"]);
                        $("#modalFormCourse input").removeAttr("disabled");
                    }
                });
            });
            
            $("#close").click(function(e){  
                e.preventDefault();
                $("#modalCourse").css("display","none");
            });
            
            $(".editCompanyItem").click(function(e){
                e.preventDefault();
                activeEdit = $(this);
                $("#modalCompany").css("display","block");
                var parent = $(this).parent().parent();
                var course = parent.children(".company").text();
                $("#modalFormCompany .company").val(course);
            });
           
            $("#modalFormCompany").submit(function(e){
                e.preventDefault();
                var x = $(this).serialize();
                $("#modalFormCompany input").attr("disabled", "disabled" );
                $.post("/index.php/maintenance/updateCompanyName", { x1: x }, function(data, status){
                    var json= JSON.parse(data);
                    if (status === "success"){
                        $("#modalCompany").css("display","none");
                        var parent = activeEdit.parent().parent();
                        parent.children(".company").text(json["companyName"]);
                        parent.children(".companySlug").text(json["companySlug"]);
                        $("#modalFormCompany input").removeAttr("disabled");
                    }
                });
            });
            
            $("#close2").click(function(e){  
                e.preventDefault();
                $("#modalCompany").css("display","none");
            });
            
            $(".contactRef").click(function(e){  
                e.preventDefault();
                var ref = $(this).attr("data-ref");
                alert(ref);
                $.get("/index.php/maintenance/markContactRead", {"ID" : ref}, function(data,status){
                    $(this).parent().fadeOut();
                });
            });
        });
    </script>
    
</body>
</html>