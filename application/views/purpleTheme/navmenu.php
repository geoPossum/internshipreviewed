    <div id="navigation">
        <div class="menu-area">
            <div class="content">
                <div class="branding">
                    <a href="/"><?php echo $siteTitle; ?></a>
                </div>
                <div class="menu"> 
                    <a class='menu-item br' href="/index.php/adverts/"> 
                        <i class="fa fa-book"></i> Interns Needed</a>
                    <a class='menu-item br' href="/index.php/attachments/courses"> 
                        <i class="fa fa-book"></i> Courses</a>
                    <a class='menu-item br' href="/index.php/attachments/companies"> 
                        <i class="fa fa-institution"></i> Companies</a>
                    <a class='menu-item br' href="/index.php/mydata/"> 
                        <i class="fa fa-database"></i> Edit Data </a>
                    <?php if ($loggedIN): ?>
                    <a class='menu-item br' href="/index.php/access/logout"> 
                        <i class="fa fa-user"></i> Log Out</a>
                    <?php else: ?>
                    <a class='menu-item br' href="/index.php/access/login"> 
                        <i class="fa fa-user"></i> Log In</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
