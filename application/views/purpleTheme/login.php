<!DOCTYPE html>
<html>
<head>
	<title><?php echo $siteTitle; ?> - Login</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="George Nyoro">
	<link rel="shortcut icon" href="/bin/images/favicon.ico">
	<meta name="description" content="Are you looking for an internship? 
            Click to check out various industrial attachment experiences at 
            companies as submitted by hundreds of students.">
	<meta name="keywords" content="Attachments, Internships, Jobs, Kenya, 
            Student, Industrial Attachment, Industrial Training">
        <link rel="stylesheet" href='/bin/font-awesome-4.1.0/css/font-awesome.css'>
	<link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeOther.css">
        <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeLogin.css">

	<script type="text/javascript" src="/bin/jquery.js"></script>
</head>
<body>
    
    <?php include("navmenu.php");?>
    <div id="body">
        <div class="notice <?php if ($errorMessage==null){echo "hidden";} ?>">
            <div class="content">
                <?php echo $errorMessage; ?>
            </div>
        </div>
        <div class="content">
            <div class="login-item facebook">
                <a href="<?php echo $facebookURL;?>" class=""><i class="fa fa-facebook fa-2x"></i> Login Using Facebook</a>
            </div>
            <div class="login-item google hidden">
                <a href="#" class=""><i class="fa fa-google fa-2x"></i> Login Using Google</a>
            </div>
            <div class="note">
                <div>The email associated with the login should be the same one used throughout
                your interactions in the site.</div>
            </div>
        </div>
    </div>
</body>
</html>