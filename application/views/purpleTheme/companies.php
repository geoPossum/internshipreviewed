<!DOCTYPE html>
<html>
<head>
    <title><?php echo $siteTitle; ?>- Companies</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="George Nyoro">
    <link rel="shortcut icon" href="/bin/images/favicon.ico">
    <meta name="description" content="Are you looking for an internship? 
        Click to check out various industrial attachment experiences at 
        companies as submitted by hundreds of students.">
    <meta name="keywords" content="Attachments, Internships, Jobs, Kenya, 
        Student, Industrial Attachment, Industrial Training">
    <link rel="stylesheet" href='/bin/font-awesome-4.1.0/css/font-awesome.css'>
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeOther.css">
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeCourses.css">

    <script type="text/javascript" src="/bin/jquery.js"></script>
</head>
<body>
    
    <?php include("navmenu.php");?>
    
    <div id="body">
        <div class="content">
            <div class="pre-text"> These are the companies that others have 
                submitted on this site. Feel free to <a href="/">contribute</a> data yourself
                for a course not listed.
            </div>
            <div class="table-data companies">
                <table class='hide-pay'>
                    <thead>
                        <th> <i class="fa fa-link purple"></i> Company </th>
                        <th> <i class="fa fa-bar-chart-o purple"></i> Location</th>
                        <th> <i class="fa fa-money purple"></i> Pay</th>
                    </thead>
                    <tbody>
                        <?php foreach ($companies as $company): ?>
                        <tr>
                            <td><a href="/index.php/attachments/<?php echo $company["link"]; //echo "item/".$company["SubmissionID"]; ?>"><?php echo ucwords($company["companyName"]); ?></a></td>
                            <td><?php echo ucwords($company["location"]); ?></td>
                            <td><?php echo $company["pay"]; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
