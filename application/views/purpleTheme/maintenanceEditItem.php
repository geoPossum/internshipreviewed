<!DOCTYPE html>
<html>
<head>
    <title><?php echo $siteTitle; ?> - Maintenance</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="George Nyoro">
    <link rel="shortcut icon" href="/bin/images/favicon.ico">
    <meta name="description" content="Are you looking for an internship? 
        Click to check out various industrial attachment experiences at 
        companies as submitted by hundreds of students.">
    <meta name="keywords" content="Attachments, Internships, Jobs, Kenya, 
        Student, Industrial Attachment, Industrial Training">
    <link rel="stylesheet" href='/bin/font-awesome-4.1.0/css/font-awesome.css'>
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeOther.css">
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeMaintenanceEditItem.css">

    <script type="text/javascript" src="/bin/jquery.js"></script>
</head>
<body>
    
    <?php include("navmenu.php"); ?>

    <div id="body">
        <?php if (!empty($status)): ?>
        <div id="status"><?php echo $status; ?>
        </div>
        <?php endif; ?>
        
        <div id="new-form">
           <form method="POST" action="/index.php/maintenance/editUserdata/<?php echo $userData["SubmissionID"]; ?>" class="form br">
                <div class="questions">
                    <input type="hidden" name="SubmissionID" value="<?php echo $userData["SubmissionID"]; ?>">
                    <input type="hidden" name="userID" value="<?php echo $userData["userID"]; ?>">
                    <div class="question course">
                       <div class="text">Course:</div>
                       <div class="input">
                           <input type="text" name="course" value="<?php echo $userData["course"]; ?>">
                       </div>
                   </div>

                   <div class="question company-name">
                       <div class="text">Name of Company:</div>
                       <div class="input">
                           <input type="text" name="companyName" value="<?php echo $userData["companyName"]; ?>">
                       </div>
                   </div>

                   <div class="question location">
                       <div class="text">Company Location:</div>
                       <div class="input">
                           <input type="text" name="location" value="<?php echo $userData["location"]; ?>">
                       </div>
                   </div>

                   <div class="question pay">
                       <div class="text">Pay:</div>
                       <div class="input">
                           <input type="text" name="pay" value="<?php echo $userData["pay"]; ?>">
                       </div>
                   </div>

                   <div class="question skills">
                       <div class="text">Skills Gained:</div>
                       <div class="input">
                           <input type="text" name="skills" value="<?php echo $userData["skills"]; ?>">
                       </div>
                   </div>

                   <div class="question experience">
                       <div class="text">Comment on overall experience: </div>
                       <div class="input">
                           <textarea name="experience"><?php echo $userData["experience"]; ?></textarea>
                       </div>
                   </div>

                   <div class="submit">
                       <div class="button">
                           <i class="fa fa-paper-plane"></i> <input type="submit" name="editSubmission" value="Submit"/> 
                       </div>
                   </div>

               </div>
           </form>
       </div>
        
        <a href="/index.php/maintenance/">
            <div class="back"> <i class="fa fa-backward"></i> Back to Maintenance</a>
            </div>  
    </div>
    
    <?php // var_dump($userData);?>