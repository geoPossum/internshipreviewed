<!DOCTYPE html>
<html>
<head>
    <title><?php echo $siteTitle; ?> - Courses</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="George Nyoro">
    <link rel="shortcut icon" href="/bin/images/favicon.ico">
    <meta name="description" content="Are you looking for an internship? 
        Click to check out various industrial attachment experiences at 
        companies as submitted by hundreds of students.">
    <meta name="keywords" content="Attachments, Internships, Jobs, Kenya, 
        Student, Industrial Attachment, Industrial Training">
    <link rel="stylesheet" href='/bin/font-awesome-4.1.0/css/font-awesome.css'>
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeOther.css">
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeAdItem.css">

    <script type="text/javascript" src="/bin/jquery.js"></script>
</head>
<body>
    <?php include("navmenu.php");?>
    
    <div id="body">
        <div class='courseItem'>
            <div class='company-left'>
                <div class="co-background"></div>
                <div class='content'>
                    <div>
                        <div class='name desc-item'>
                            <div class='text'><i class="fa fa-institution"></i>
                                <?php echo $item[0]["companyName"]; ?>
                            </div>
                        </div>
                        <div class='location desc-item'>
                            <div class='text'> <i class="fa fa-location-arrow"></i> Location:</div>
                            <div class='value'><?php echo $item[0]["location"]; ?></div>
                        </div>
                        <div class='courses desc-item'>
                            <div class='text'> <i class="fa"></i> Courses That Can Apply:</div>
                            <ul>
                            <?php foreach(explode(",",$item[0]["courses"]) as $course):?>
                            <li class='value'><?php echo $course; ?></li>
                            <?php endforeach;?>
                            </uL>
                        </div>
                    </div>
                </div>  
            </div>

            <div class="company-right">
                <div class='content'>
                    <div class="inner">
                       <div id="blog">
                           <?php echo nl2br($item[0]['blog']) ?>
                       </div>
                        <div id="contact">
                            <div class="topic">Contact Us</div>
                            <div><?php echo nl2br($item[0]['contacts']); ?></div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>