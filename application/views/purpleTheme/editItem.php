<!DOCTYPE html>
<html>
<head>
	<title><?php echo $siteTitle; ?> - Edit an Item</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="George Nyoro">
	<link rel="shortcut icon" href="/bin/images/favicon.ico">
	<meta name="description" content="Are you looking for an internship? 
            Click to check out various industrial attachment experiences at 
            companies as submitted by hundreds of students.">
	<meta name="keywords" content="Attachments, Internships, Jobs, Kenya, 
            Student, Industrial Attachment, Industrial Training">
        <link rel="stylesheet" href='/bin/font-awesome-4.1.0/css/font-awesome.css'>
	<link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeOther.css">
        <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeEditItem.css">
        <script type="text/javascript" src="/bin/trackingcode.js"></script>
	<script type="text/javascript" src="/bin/jquery.js"></script>
        <script type="text/javascript" src="/bin/js/editItem.js"></script>
</head>
<body>
    <?php include("navmenu.php");?>
    
    <div id="body">
        <div class="heading">
            <div class="bg"></div>
            <div class="content">
                Edit an Entry
            </div>
        </div>
        
        <div class="content">
            <div class="edit-left">
                <div>
                    <div class="topic">Choose Entry to Edit</div>
                    <div class="inner-list">
                        <?php foreach($editMenuItems as $item): ?>
                        <div class="list-item">
                            <a href="/index.php/mydata/getData/<?php echo $item["SubmissionID"] ;?>"> 
                                <div class="bolden"><?php echo $item["companyName"];?></div>
                                <?php echo $item["course"]; ?>
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>


            <div class="edit-right">
                <div class="content">
                    <form method="GET" action="/index.php/mydata/updateData">
                        <div class="hidden checkValue">
                            <input type="hidden" name="checkValue" value="">
                        </div>
                        <div class="top section">
                            
                            <div class='input-field course'>
                                <div class='title'> Course
                                </div>
                                <div class='input'>
                                     <input type="text" name="course" value=''>
                                </div>
                            </div>
                            
                            <div class='input-field companyName'>
                                <div class='title'>Company Name
                                </div>
                                <div class='input'>
                                     <input type="text" name="companyName" value=''>
                                </div>
                            </div>
                            
                            <div class='input-field location'>
                                <div class='title'>Location
                                </div>
                                <div class='input'>
                                     <input type="text" name="location" value=''>
                                </div>
                            </div>
                            
                        </div>
                        
                         <div class="bottom section">
                            
                            <div class='input-field pay'>
                                <div class='title'> Pay
                                </div>
                                <div class='input'>
                                     <input type="text" name="pay" value=''>
                                </div>
                            </div>
                            
                            <div class='input-field skills'>
                                <div class='title'>Skills Gained: separate by comma's (,)
                                </div>
                                <div class='input'>
                                     <input type="text" name="skills" value=''>
                                </div>
                            </div>
                            
                            <div class='input-field experience'>
                                <div class='title'>Experience: Comment, Rant on
                                    what you liked, disliked
                                </div>
                                <div class='input'>
                                    <textarea name="experience"></textarea>
                                </div>
                            </div>
                             
                             <div class='input-field submit'>
                                 <div class='input'>
                                     <input type="submit" value="Submit">
                                 </div>
                             </div>
                            
                        </div>
                    </form>
                </div>
            </div> 
        </div>
    </div>
    
</body>
</html>