<!DOCTYPE html>
<html>
<head>
    <title><?php echo $siteTitle; ?> - Courses</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="George Nyoro">
    <link rel="shortcut icon" href="/bin/images/favicon.ico">
    <meta name="description" content="Are you looking for an internship? 
        Click to check out various industrial attachment experiences at 
        companies as submitted by hundreds of students.">
    <meta name="keywords" content="Attachments, Internships, Jobs, Kenya, 
        Student, Industrial Attachment, Industrial Training">
    <link rel="stylesheet" type="text/css" href="/bin/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href='/bin/font-awesome-4.1.0/css/font-awesome.css'>
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeOther.css">
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeMaintenance.css">
    <script type="text/javascript" src="/bin/jquery.js"></script>
    <script type="text/javascript" src="/bin/jquery.js"></script>
   
</head>
<body>
    <?php include("navmenu.php");?>
    
    <div id="body">
        <?php if (!empty($message)):?>
        <div class="message"><?php echo $message; ?> </div>
        <?php endif; ?>
        
        <!-- tabs-->
        <div id="tabs">
            <ul class="nav nav-pills" role="tablist">
                <li role="presentation">
                    <a class="link" data-next="newItems-page"  href="#">New Items <span class="badge"><?php echo $unvalidatedBadge; ?></span>
                    </a>
                </li>
                <li role="presentation">
                    <a class="link " data-next="contacts-page" href="#">Contacts <span class="badge"><?php echo $contactedBadge; ?></span> 
                    </a>
                </li>
                <li role="presentation"><a class="link " data-next="courses-page" href="#">Courses</a></li>
                <li role="presentation"><a class="link " data-next="companies-page" href="#">Companies</a></li>
                <li role="presentation" class="active"><a class="link " data-next="upload-page" href="#">Upload New Data</a></li>
            </ul>
        </div>
        
        
        
        <div class="page" id="newItems-page">New Items</div>
        
        <div class="page" id="contacts-page">Contacts</div>
        
        <div class="page" id="courses-page">Courses</div>
        
        <div class="page" id="companies-page">Companies</div>
        
        <div class="page nohide" id="upload-page" >
            
            <form method="POST" action="/index.php/maintenance/uploadFile" id="uploadItems" 
                  enctype="multipart/form-data">
                <div>Choose CSV file to upload: </div>
                <input type="file" name="userfile" >
                <input type="submit" name="submit" value="Upload"> 
            </form>
        </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function(){
            $(".link").click(function(event){
//                event.preventDefault();
                $(".link").parent("li").removeClass("active");
                var next = $(this).attr("data-next");
                $(".page").css("display", "none");
                $("#"+next).css("display", "block");
                $(this).parent().addClass("active");
            });
            
            
        });
    </script>
</body>
</html>