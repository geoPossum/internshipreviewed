<!DOCTYPE html>
<html>
<head>
    <title><?php echo $siteTitle; ?> - <?php echo $item[0]["companyName"]; ?> </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="George Nyoro">
    <link rel="shortcut icon" href="/bin/images/favicon.ico">
    <meta name="description" content="Are you looking for an internship? 
        Click to check out various industrial attachment experiences at 
        companies as submitted by hundreds of students.">
    <meta name="keywords" content="Attachments, Internships, Jobs, Kenya, 
        Student, Industrial Attachment, Industrial Training">
    <link rel="stylesheet" href='/bin/font-awesome-4.1.0/css/font-awesome.css'>
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeOther.css">
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeCourseItem.css">

    <script type="text/javascript" src="/bin/jquery.js"></script>
</head>
<body>
    
    <?php include("navmenu.php");?>
     
    <div id="body">
        <div class='courseItem'>
            <div class='company-left'>
                <div class="co-background"></div>
                <div class='content'>
                    <div>
                        <div class='name desc-item'>
                            <div class='text'><i class="fa fa-institution"></i>
                                <?php echo $item[0]["companyName"]; ?>
                            </div>
                        </div>
                        <div class='location desc-item'>
                            <div class='text'> <i class="fa fa-location-arrow"></i> Location:</div>
                            <div class='value'><?php echo $item[0]["location"]; ?></div>
                        </div>
                        <div class='course desc-item'>
                            <div class='text'><i class="fa fa-book"></i> Course:</div>
                            <div class='value'><?php echo $item[0]["course"]; ?></div>
                        </div>
                        <div class='pay desc-item'>
                            <div class='text'> <i class="fa fa-money"></i> Average Pay:</div>
                            <div class='value'><?php echo $item[0]["pay"]; ?> </div>
                        </div>
                    </div>
                </div>  
            </div>

            <div class="company-right">
                <div class='content'>
                    <div class="inner">
                        <?php foreach($submissions as $index => $s): ?>
                        <div class="experience-item">
                            <div class="topic"><i class="fa fa-compass"></i> Experience Entry #<?php echo $index+1; ?> </div>
                            <div class="skills">
                                <div class="skill-topic">Skills: </div>
                                <div class="skill-body">
                                    <?php echo nl2br($s["skills"]); ?>
                                </div>
                            </div>
                            <div class="message">
                                <div class="message-topic">Experience: </div>
                                <div class="message-body">
                                    <i class="fa fa-comment-o"></i> 
                                    <?php echo nl2br($s["experiences"]); ?>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
