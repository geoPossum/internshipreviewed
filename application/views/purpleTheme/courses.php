<!DOCTYPE html>
<html>
<head>
    <title><?php echo $siteTitle; ?> - Courses</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="George Nyoro">
    <link rel="shortcut icon" href="/bin/images/favicon.ico">
    <meta name="description" content="Are you looking for an internship? 
        Click to check out various industrial attachment experiences at 
        companies as submitted by hundreds of students.">
    <meta name="keywords" content="Attachments, Internships, Jobs, Kenya, 
        Student, Industrial Attachment, Industrial Training">
    <link rel="stylesheet" href='/bin/font-awesome-4.1.0/css/font-awesome.css'>
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeOther.css">
    <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeCourses.css">

    <script type="text/javascript" src="/bin/jquery.js"></script>
</head>
<body>
    
    <?php include("navmenu.php");?>
     
    <div id="body">
        <div class="content">
            <div class="pre-text"> These are the courses that others have 
                submitted on this site. Feel free to <a href="/">contribute</a> data yourself
                for a course not listed.
            </div>
            <div class="table-data courses">
                <table>
                    <thead>
                        <th> <i class="fa fa-link purple"></i> Course Name </th>
                        <th> <i class="fa fa-bar-chart-o purple"></i>  No of Companies Listed for Course</th>
                    </thead>
                    <tbody>
                        <?php foreach($courses as $course): ?>
                            <tr>
                                <td><a href="/index.php/attachments/<?php echo $course["link"];?>"><?php echo $course["course"];?></a></td>
                                <td><?php echo ucwords($course["noCoursesListed"]);?></td>
                            </tr>
                        <?php endforeach;?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
