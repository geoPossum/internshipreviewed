<!DOCTYPE html>
<html>
<head>
	<title><?php echo $siteTitle; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="George Nyoro">
	<link rel="shortcut icon" href="/bin/images/favicon.ico">
	<meta name="description" content="Are you looking for an internship? 
            Click to check out various industrial attachment experiences at 
            companies as submitted by hundreds of students.">
	<meta name="keywords" content="Attachments, Internships, Jobs, Kenya, 
            Student, Industrial Attachment, Industrial Training">
        <link rel="stylesheet" href="/bin/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href='/bin/font-awesome-4.1.0/css/font-awesome.css'>
        <link rel="stylesheet" type="text/css" href="/bin/css/purpleThemeHome.css">
        <script type="text/javascript" src="/bin/trackingcode.js"></script>
	<script type="text/javascript" src="/bin/jquery.js"></script>
</head>
<body>
    
    <div id="navigation">
        <div class="background"></div>
        <div class="content">
            <div class="menu-area">
                <div class="branding">
                    <a href="/"><?php echo $siteTitle; ?></a>
                </div>
                <div class="menu"> 
                    <a class='menu-item br' href="/index.php/adverts/"> 
                        <i class="fa fa-book"></i> Interns Needed</a>
                    <a class='menu-item br' href="/index.php/attachments/courses"> <i class="fa fa-book"></i> Courses</a>
                    <a class='menu-item br' href="/index.php/attachments/companies"> <i class="fa fa-institution"></i> Companies</a>
                    <a class='menu-item br' href="/index.php/mydata/"> <i class="fa fa-database"></i> My Submissions</a>
                    <?php if ($loggedIN): ?>
                    <a class='menu-item br' href="/index.php/access/logout"> <i class="fa fa-user"></i> Log Out</a>
                    <?php else: ?>
                    <a class='menu-item br' href="/index.php/access/login"> <i class="fa fa-user"></i> Log In</a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="jumbo-area">
                <div class="<?php if ($failedCaptcha or $errorMessage){ echo "hidden";};?>">
                    Find reviews by others on hundreds of companies that you can
                    consider for internship.
                </div>
                <div class="<?php if (!$failedCaptcha){ echo "hidden";}else { echo "error";}?>">
                    Verify You are Human!: <br/>Answer the calculation on the Form
                </div>
                <div class="<?php if (!$errorMessage){ echo "hidden";}else { echo "error";}?>">
                    <?php echo $errorMessage; ?>
                </div>
            </div>
        </div>
    </div>
    
    
    <div id="new-form">
        <form method="POST" action="/" class="form br">
            <div class="heading">
                How was your Internship? <i class="fa fa-file"></i> 
            </div>
            <div class="questions">
                <div class="question course">
                    <div class="text">What Course Do You Take?</div>
                    <div class="input">
                        <input type="text" name="course" value="<?php echo $failedSubmit["course"];?>">
                    </div>
                </div>

                <div class="question company-name">
                    <div class="text">Name of Company:</div>
                    <div class="input">
                        <input type="text" name="companyName" value="<?php echo $failedSubmit["companyName"];?>">
                    </div>
                </div>

                <div class="question location">
                    <div class="text">Company Location:</div>
                    <div class="input">
                        <input type="text" name="location" value="<?php echo $failedSubmit["location"];?>">
                    </div>
                </div>

                <div class="question pay">
                    <div class="text">Pay:</div>
                    <div class="input">
                        <input type="text" name="pay" value="<?php echo $failedSubmit["pay"];?>">
                    </div>
                </div>

                <div class="question skills">
                    <div class="text">Skills Gained: (e.g Welding, Communication, Management)</div>
                    <div class="input">
                        <input type="text" name="skills" value="<?php echo $failedSubmit["skills"];?>">
                    </div>
                </div>
                
                <div class="question experience">
                    <div class="text">Comment on overall experience, likes, dislikes (e.g. Staff was friendly. Lunch provided...)</div>
                    <div class="input">
                        <textarea name="experience"><?php echo $failedSubmit["experience"];?></textarea>
                    </div>
                </div>
                
                <div class="question captcha">
                    Verify you are human:
                    <div class="text"><?php echo $captcha["statement"]; ?> = </div>
                    <div class="input">
                        <input type="text" name="captcha">
                    </div>
                </div>
                
                <div class="submit">
                    <div class="button">
                        <i class="fa fa-paper-plane"></i> <input type="submit" name="new_submission" value="Submit"/> 
                    </div>
                </div>
                
            </div>
        </form>
    </div>
    
    <div id="body">
        <div class="content">
            <div class="section companies-looking company-list <?php if (empty($advertCompanies)){echo "no-looking";} ?> ">
                <div>
                    <div class="heading">
                        Companies Looking For Interns <a class="link" href="/index.php/adverts">(See More) </a>
                    </div>
                    
                    <div class='no-look'>
                        <div class='text'>
                            No companies are looking as of now. Check back later.
                        </div>
                    </div>
                    <div class="list">
                        <?php foreach($advertCompanies as $index => $advertCompany): ?>
                        <a class="list-item" href="/index.php/adverts/seeAd/<?php echo $advertCompany["advertID"]; ?>">
                            <div class="hover-image">
                                <div class="inner">
                                    <strong>View</strong><br><br>
                                    <i class="fa fa fa-link fa-3x"></i>
                                </div>
                            </div>
                            <div class="co-image">
                                <i class="fa fa fa-building fa-2x"></i>
                            </div>
                            <div class="title">
                                <?php echo $advertCompany["companyName"]; ?>
                            </div>
                            <div class='location'>
                                <i class="fa fa-location-arrow"></i> <?php echo $advertCompany["location"]; ?>
                            </div>
                            <div class="courses">
                                <div class="text"><?php  echo sizeof($advertCompany["courses"]); ?> Course<?php if (sizeof($advertCompany["courses"]) > 1){echo "s";}?> Applicable:
                                </div>
                                <div class="course-list">
                                    <?php foreach($advertCompany["courses"] as  $course_inner): ?>
                                    <div> <div class="fa fa-tag"></div> <div> <?php echo $course_inner; ?></div> </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </a>
                        <?php endforeach; ?>
                        
                    </div>
                </div>
            </div>
            
            <div class="section top-companies company-list">
                <div>
                    <div class="heading">
                        Top Rated Companies
                    </div>
                    <div class="list">
                        <?php foreach($topRatedCompanies as $topRatedC): ?>
                        <a href="<?php echo "/index.php/attachments/companies/".$topRatedC["companySlug"];?>" class="list-item">
                            <div class="hover-image">
                                <div class="inner">
                                    <i class="fa fa fa-link fa-4x"></i>
                                </div>
                            </div>
                            <div class="co-image">
                                <i class="fa fa-building fa-2x"></i>
                            </div>
                            <div class="title">
                                <?php echo $topRatedC["companyName"]; ?>
                            </div>
                            <div class='location'>
                                <i class="fa fa-location-arrow"></i> 
                                    <?php echo $topRatedC["location"]; ?>
                            </div>
                            <div class="courses">
                                <div class="text"><?php  echo sizeof($topRatedC["courses"]); ?> Course<?php if (sizeof($topRatedC["courses"]) > 1){echo "s";}?> Applicable:
                                </div>
                                <div class="course-list">
                                    <?php foreach($topRatedC["courses"] as $course_2): ?>
                                    <div><div class="fa fa-tag"></div> <div class="d1"> <?php echo $course_2; ?></div> </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </a>
                        <?php endforeach; ?>
                        
                    </div>
                </div>
            </div>
            
            <div class="section contact-us">
                <div>
                    <div class="heading">
                        Contact Us
                    </div>
                    <div class="form">
                        <form method="POST" action="/">
                            <div class="form-item email">
                                <div class="text">Email:</div>
                                <div class="input">
                                    <input type="email" name="contactemail" value="<?php echo $failedContact["email"]; ?> ">
                                </div>
                            </div>
                            
                            <div class="form-item subject">
                                <div class="text">Subject:</div>
                                <div class="input">
                                    <input type="text" name="contactsubject" value="<?php echo $failedContact["subject"]; ?> ">
                                </div>
                            </div>
                            
                            
                            <div class="form-item captcha">
                                Verify you are human:
                                <div class="text"><?php echo $captcha["statement"]; ?> =</div>
                                <div class="input">
                                    <input type="text" name="captcha"> 
                                </div>
                            </div>
                            
                            <div class="form-item message">
                                <div class="text">Message:</div>
                                <div class="input">
                                    <textarea name="contactmessage"><?php echo $failedContact["message"]; ?></textarea>
                                </div>
                            </div>
                            
                            <div class="form-item submit">
                                <div class='button'>
                                   <i class="fa fa-paper-plane"></i>  <input type="submit" name="contactSubmit" value='Submit'>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    
    <?php if ($admin):?>
    <a id="maintenance" href="/index.php/maintenance"> Maintenance</a>
    <?php endif; ?>
</body>
</html>