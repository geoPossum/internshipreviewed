<div class="row">
	<div class="container thanks text-center">
		<p>Thank you for completing the survey. Feel free to go <a href="/index.php/survey/">another round</a>.</p>
		<p>Also, you can now <a href="/index.php/access/login">login</a> and have access to tons of stuff!</p>
	</div>
</div>