    <?php
    $survey = $this->session->userdata("survey");
    function generate_input($array, $survey, $name){
                foreach ($array as $number => $item){
                          echo "    
                    <span class=\"input-group-addon\"> 
                      <input type=\"radio\" name=\"survey_$name\" value=\"". number_format((float)($number+1)/count($array), 2, '.', ''). "\"";
                          if (  number_format((float)($number+1)/count($array), 2, '.', '') == $survey["survey_$name"] ){
                          echo " checked";
                      }
                      echo ">$item</span>";
                      }
                // create new line
                      echo "
                      ";
    }
    ?>

    <style type="text/css">
      .text-muted:hover{
        color:#999;
      }
    </style>

<div class="container">
    <form id="submit_form" style="margin-top:1em;" role="role" method="POST" action="/index.php/survey/p2">

      <div class="form-group">
        <div class="progress">
        <div class="progress-bar progress-bar-one active" style="width: 50%"></div>
        <div class="progress-bar progress-bar-two progress-bar-striped" style="width: 50%">
          <span style="font-weight:bold;">Part 2/2</span>
        </div>
        </div>
      </div>

      <div class="form-group">
        <div class="alert alert-login">
          Remember to fill in <strong>starred *</strong> fields.
        </div>
      </div>

      <div class="form-group">
        <span class="col-md-6">Nature of work/ What does the company do?<small>(Manufacturer of, vendor of, repairs in)</small>:</span>
        <div class="col-md-6 input-group">
          <span style="color:red;" class="input-group-addon">*</span>
          <textarea class="form-control" name="survey_nature_of_work" cols="80" rows="5" 
          ><?php $survey = $this->session->userdata("survey");
          echo $survey["survey_nature_of_work"] ?></textarea>
        </div>
      </div>

      <div class="form-group">
        <span class="col-md-6">Location (X Road, Y Place, Kitale):</span>
        <div class="col-md-6 input-group">
          <input class="form-control" type="text" name="survey_company_location" value=<?php 
          echo '"'.$survey["survey_company_location"].'"'?> />
        </div>
      </div>

      <div class="form-group">
        <?php
        if ($array_hands_on){
          echo "<div class=\"col-md-6\">Are/Were you given hands-on work to do or did you mostly hang around watching the professionals do their work or any other?:</div>
                <div class=\"input-group col-md-6\">";
                  echo '
                  <span style="color:red;" class="input-group-addon">*</span>';
                    
                    $array = $array_hands_on;
                    generate_input($array, $survey, "hands_on");
                    echo "
              </div><!--input-group-->";
        }
        ?>
      </div>

      <div class="form-group">
        <?php 
        if ($array_interesting){
          echo "
            <div class=\"col-md-6\">Was it interesting?</div>
                  <div class=\"input-group col-md-6\">";
                  echo '<span style="color:red;" class="input-group-addon">*</span>';
                    
                    $array = $array_interesting;
                    generate_input($array, $survey, "interesting");

                  echo "</div><!--input-group-->";
        }
        ?>
      </div>

      <div class="form-group">
          <?php
          if ($array_days_of_working){
            echo "<span class=\"col-md-6\">Days of Working:</span>
                <div class=\"input-group col-md-6\">";

                $array = $array_days_of_working;
                foreach ($array_days_of_working as $number => $item){
                          echo "    
                          <span class=\"input-group-addon\"> 
                          <input type=\"radio\" name=\"survey_working_days\" value=\"". number_format((float)($number+1)/count($array), 2, '.', ''). "\"
        ";
                          if (  number_format((float)($number+1)/count($array), 2, '.', '') == $survey["survey_working_days"] ){
                          echo " checked";
                      }
                      echo(">$item</span>");
                      }    

                echo "</div><!-- /input-group -->";
           }
          ?> 
      </div>

      <div class="form-group">
        <span class="col-md-6">Was there any creative input required of you? Did you make something while there? Were you challenged to come up with solutions?</span>
        <div class="col-md-6 input-group">  
          <input class="form-control" type="text" name="survey_innovation" value=<?php echo '"'.$survey["survey_innovation"].'"'?> />
        </div>
      </div>


        <div class="form-group">
        <?php
          if ($array_dress_code){
            echo "<span class=\"col-md-6\">Dress Code:</span>
              <div class=\"input-group col-md-6\">";
                  
              $array = $array_dress_code;
              foreach ($array_dress_code as $number => $item){
                        echo "    
                        <span class=\"input-group-addon\"> 
                        <input type=\"radio\" name=\"survey_dress_code\" value=\"". number_format((float)($number+1)/count($array), 2, '.', ''). "\"
      ";
                        if (  number_format((float)($number+1)/count($array), 2, '.', '') == $survey["survey_dress_code"] ){
                        echo " checked";
                    }
                    echo(">$item</span>");
                    }

                echo "</div><!-- /input-group -->";
          }
        ?>
      </div>

      <div class="form-group">
        <span class="col-md-6">Default Duration of Attachment offered 
          <small>(e.g. 2 months, 12 weeks with possibility of extension, infinite):</small></span>
          <div class="col-md-6 input-group">
            <input class="form-control col-md-6" type="text" name="survey_maximum_duration" value=<?php 
            $survey = $this->session->userdata("survey");
            echo '"'.$survey["survey_maximum_duration"].'"'?> />
          </div>
      </div>
      
      <div class="form-group">
        <?php
        if ($array_company_size){
          echo "<span class=\"col-md-6\">Company Size:</span>

            <div class=\"input-group col-md-6\">";
              $array = $array_company_size;
                foreach ($array as $number => $item){
                          echo "    
                          <span class=\"input-group-addon\"> 
                          <input type=\"radio\" name=\"survey_company_size\" value=\"". number_format((float)($number+1)/count($array), 2, '.', ''). "\"
        ";
                          if (  number_format((float)($number+1)/count($array), 2, '.', '') == $survey["survey_company_size"] ){
                          echo " checked";
                      }
                      echo(">$item</span>");
                      }         
              echo "</div><!-- /input-group -->";
        }
        ?>
        </div>

      <div class="form-group">
        <span class="col-md-6">Calendar Year attachment was undertaken(e.g 2014):</span>
        <div class="col-md-6 input-group">
          <span class="input-group-addon"></span>
          <input class="form-control col-xs-12 col-md-6" type="text" name="survey_calendar_year_attached" placeholder="2014" 
          value=
          <?php
          $survey = $this->session->userdata("survey");
          echo '"'.$survey["survey_calendar_year_attached"].'"';
          ?>
          />
        </div>
      </div>

      <div class="form-group">
        <span class="col-md-6">Course Stage(e.g. end of 3rd Year):</span>
        <div class="col-md-6 input-group">
          <span class="input-group-addon"></span>
          <input class="form-control col-xs-12 col-md-6" type="text" name="survey_academic_year_attached" placeholder="4th year" 
          value=
          <?php
          $survey = $this->session->userdata("survey");
          echo '"'.$survey["survey_academic_year_attached"].'"';
          ?>
          />
        </div>
      </div>
     
   

      <div class="form-group">
        <div class="col-md-offset-6 col-md-6 input-group">
          <input type="submit" class="btn btn btn-next-survey-page col-xs-6"  name="survey_submit2"  value="SUBMIT" />
        </div>
      </div>     
    </form>