<?php

  $survey = $this->session->userdata("survey");
  function generate_input($array, $survey, $name){
              foreach ($array as $number => $item){
                        echo "    
                  <span class=\"radio-inline\"> 
                    <input type=\"radio\" name=\"survey_$name\" value=\"". number_format((float)($number+1)/count($array), 2, '.', ''). "\"";
                        if (  number_format((float)($number+1)/count($array), 2, '.', '') == $survey["survey_$name"] ){
                        echo " checked";
                    }
                    echo ">$item</span>";
                    }
              // create new line
                    echo "
                    ";
  }
?>
<style type="text/css">
    .text-muted:hover{
      color:#999;
    }
</style>
  
  <div class="container">
    <form id="submit_form" style="margin-top:1em;" role="role" method="POST" action="/index.php/survey/p1"> 
      <div class="progress">
        <div class="progress-bar progress-bar-one active" style="width: 50%"> 
          <span style="font-weight:bold;">Part 1/2</span>
        </div>
      </div>
      <?php if ( isset($error_message) && $error_message ){ ?>
        <div class="alert alert-login" role="alert">
          <?php echo $error_message; ?>
        </div>
      <?php } ?>
      <div class="alert alert-login">
        Kindly fill in all <strong>*starred</strong> fields.
      </div>

      <div class="form-group ">
      <?php
          $survey = $this->session->userdata("survey");
          if (!isset($survey["survey_email"]) || !$survey["survey_email"] ){
            echo<<<EOD
          <span class="col-md-6"></span>
          <div class="col-md-6 input-group">
            <input type="submit" class="btn btn btn-danger next" style="width:170px;" name="survey_google_login" value="Login" />
          </div>
EOD;
          }
          else{
            echo "  <div class=\"col-md-6 col-md-offset-6 input-group\">";
            echo "
            <h4><label class=\"label label-logged-in\">Logged in as $survey[survey_email]</label><a href='/index.php/access/logout' style='color:black;' ><span class='glyphicon glyphicon-remove' aria-hidden=;true'></span></a></h4>
            ";
          }
      ?>
      </div>

      <div class="form-group">
        <span class="col-md-6">Course:</span>
        <div class="col-md-6 input-group">
          <span style="color:red;" class="input-group-addon">*</span>
          <input class="form-control" type="text" name="survey_course" placeholder="Mechatronics Engineering"
          value=
          <?php
          $survey = $this->session->userdata("survey");
          echo '"'.$survey["survey_course"].'"';
          ?>
          />
        </div>
      </div>

      <div class="form-group">
        <span class="col-md-6"> Enter Full Company Name: </span>
        <div class="col-md-6 input-group">
          <span style="color:red;" class="input-group-addon">*</span>
          <input class="form-control" type="text" name="survey_company_name" value=<?php 
          echo '"'.$survey["survey_company_name"].'"'?>/>
        </div>
      </div>

      <div class="form-group">
        <span class="col-md-6">Pay per month<small>(0/=,5000/=,-5000/= if I paid to get in):</small></span>
        <div class="input-group col-md-6">
          <input class="form-control" type="text" name="survey_pay" value=<?php 
          $survey = $this->session->userdata("survey");
          echo '"'.$survey["survey_pay"].'"'?> />
        </div>
      </div>

      <div class="form-group">
        <?php
        if ($array_redo){
          echo "<div class=\"col-md-6\">Would you recommend the experience to someone in a related course or year?</div>
              <div class=\"input-group col-md-6\"> ";
              echo '<span style="color:red;">*</span>';
                $array = $array_redo;
                generate_input($array, $survey, "redo");

              echo "</div><!-- /input-group -->";
        }
        ?>
      </div>

      <div class="form-group">
        <span class="col-md-6">What skills did you learn?<small>(welding,design of, repair of,customer interaction,theory on...)</small></span>
        <div class="input-group col-md-6">
          <span style="color:red;" class="input-group-addon">*</span>
          <textarea class="form-control" name="survey_skills" cols="80" rows="5"><?php 
            echo $survey["survey_skills"] ?></textarea>
        </div>
        <br>
      </div>

      <div class="form-group">
        <span class="col-md-6">What can you say of the experience?<small>(Tell us more. Talk, or rant if you want, comment...)</small></span>
        <div class="col-md-6 input-group">
          <textarea class="form-control" name="survey_testimony" cols="80" rows="5"><?php 
          echo $survey["survey_testimony"] ?></textarea>
        </div>
      </div>

      <div class="form-group">
        <div class="col-md-offset-6 col-md-6 input-group">
          <input type="submit" class="btn btn btn-next-survey-page" style="width:170px;" name="survey_submit1" value="Next" />
        </div>
      </div>

    </form>

    <div class="modal " id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">Kindly Login</h4>
              </div>
              <div class="modal-body">
                  You may proceed if you wish, and the data will be submitted anonymously, but:
                  <ul>
                    <li>You can never <strong>edit </strong> it after submission</li>
                    <li>You will not enjoy <strong>priviledges </strong> that those who submit do.</li>
                  </ul>
                  By logging in, people will still not be able to <strong>tie you </strong> to your data. It is still presented <strong>anonymously</strong>.
              </div>
              <div class="modal-footer">
                <a class="btn login-custom" href="/index.php/access/login">Login</a>
                <button type="button" class="btn btn-brown" data-dismiss="modal">Okay</button>
          </div>
      </div>
    </div>
  </div>

  <script type='text/javascript'>
  $(document).ready(function(){
    <?php
    $survey =$this->session->userdata("survey");
      if (!$survey["survey_email"] && !$this->session->userdata("limited_company_alert_popup") ){
        echo "$('#basicModal').modal();";
      };
    ?>
  });
  </script>
  