<?php
function generate_input($array,  $name){
            foreach ($array as $number => $item){
                      echo "    
                <span class=\"radio-inline\"> 
                  <input type=\"radio\" name=\"survey_$name\" value=\"". number_format((float)($number+1)/count($array), 2, '.', ''). "\"/>";
                      
                  echo "$item</span>";
                  }
            // create new line
                  echo "
                  ";
}
?>
<div class="container">
  <form id="survey_submit_form" style="margin-top:1em;" role="role" method="POST">  
    <div class="form-group ">
      <span class="col-md-6">Email:</span>
      <div class="col-md-6 input-group">
        <input class="form-control" type="text" name="survey_email" placeholder="Mechatronics Engineering" value=""/>
      </div>
    </div>

    <div class="form-group">
      <span class="col-md-6">Course:</span>
      <div class="col-md-6 input-group">
        <span style="color:red;" class="input-group-addon">*</span>
        <input class="form-control" type="text" name="survey_course" placeholder="Mechatronics Engineering"
        value=""/>
      </div>
    </div>

    <div class="form-group">
      <span class="col-md-6"> Enter Full Company Name: </span>
      <div class="col-md-6 input-group">
        <span style="color:red;" class="input-group-addon">*</span>
        <input class="form-control" type="text" name="survey_company_name" value="" />
      </div>
    </div>

    <div class="form-group">
      <span class="col-md-6">Pay per month<small>(0/=,5000/=,-5000/= if I paid to get in):</small></span>
      <div class="input-group col-md-6">
        <input class="form-control" type="text" name="survey_pay" value="" />
      </div>
    </div>

    <div class="form-group">
      <?php
      if ($array_redo){
        echo "<div class=\"col-md-6\">Would you recommend the experience to someone in a related course or year?</div>
            <div class=\"input-group col-md-6\"> ";
            echo '<span style="color:red;">*</span>';
              $array = $array_redo;
              generate_input($array, "redo");

            echo "</div><!-- /input-group -->";
      }
      ?>
    </div>

    <div class="form-group">
      <span class="col-md-6">What skills did you learn?<small>(welding,design of, repair of,customer interaction,theory on...)</small></span>
      <div class="input-group col-md-6">
        <span style="color:red;" class="input-group-addon">*</span>
        <textarea class="form-control" name="survey_skills" cols="80" rows="5"></textarea>
      </div>
      <br>
    </div>

    <div class="form-group">
      <span class="col-md-6">What can you say of the experience?<small>(Tell us more. Talk, or rant if you want, comment...)</small></span>
      <div class="col-md-6 input-group">
        <textarea class="form-control" name="survey_testimony" cols="80" rows="5"></textarea>
      </div>
    </div>



    <div class="form-group">
      <span class="col-md-6">Nature of work/ What does the company do?<small>(Manufacturer of, vendor of, repairs in)</small>:</span>
      <div class="col-md-6 input-group">
        <span style="color:red;" class="input-group-addon">*</span>
        <textarea class="form-control" name="survey_nature_of_work" cols="80" rows="5" ></textarea>
      </div>
    </div>

    <div class="form-group">
      <span class="col-md-6">Location (X Road, Y Place, Kitale):</span>
      <div class="col-md-6 input-group">
        <input class="form-control" type="text" name="survey_company_location" value="" />
      </div>
    </div>

    <div class="form-group">
        <?php
        if ($array_days_of_working){
          echo "<span class=\"col-md-6\">Days of Working:</span>
              <div class=\"input-group col-md-6\">";

              $array = $array_days_of_working;
              foreach ($array_days_of_working as $number => $item){
                        echo "    
                        <span class=\"input-group-addon\"> 
                        <input type=\"radio\" name=\"survey_working_days\" value=\"". number_format((float)($number+1)/count($array), 2, '.', ''). "\"
      ";
                    echo(">$item</span>");
                    }    

              echo "</div><!-- /input-group -->";
         }
        ?> 
    </div>

    <div class="form-group">
      <?php
        if ($array_dress_code){
          echo "<span class=\"col-md-6\">Dress Code:</span>
            <div class=\"input-group col-md-6\">";
                
            $array = $array_dress_code;
            foreach ($array_dress_code as $number => $item){
                      echo "    
                      <span class=\"input-group-addon\"> 
                      <input type=\"radio\" name=\"survey_dress_code\" value=\"". number_format((float)($number+1)/count($array), 2, '.', ''). "\"
    ";
                      
                  echo(">$item</span>");
                  }

              echo "</div><!-- /input-group -->";
        }
      ?>
    </div>

    <div class="form-group">
      <span class="col-md-6">Calendar Year attachment was undertaken(e.g 2014):</span>
      <div class="col-md-6 input-group">
        <span class="input-group-addon"></span>
        <input class="form-control col-xs-12 col-md-6" type="text" name="survey_calendar_year_attached" placeholder="2014" 
        value="" />
      </div>
    </div>

    <div class="form-group">
      <span class="col-md-6">Course Stage(e.g. end of 3rd Year):</span>
      <div class="col-md-6 input-group">
        <span class="input-group-addon"></span>
        <input class="form-control col-xs-12 col-md-6" type="text" name="survey_academic_year_attached" placeholder="4th year" 
        value="" />
      </div>
    </div>

    <div class="form-group">
      <?php
      if ($array_hands_on){
        echo "<div class=\"col-md-6\">Are/Were you given hands-on work to do or did you mostly hang around watching the professionals do their work or any other?:</div>
              <div class=\"input-group col-md-6\">";
                echo '
                <span style="color:red;" class="input-group-addon">*</span>';
                  
                  $array = $array_hands_on;
                  generate_input($array,  "hands_on");
                  echo "
            </div><!--input-group-->";
      }
      ?>
    </div>

    <div class="form-group">
      <?php 
      if ($array_interesting){
        echo "
          <div class=\"col-md-6\">Was it interesting?</div>
                <div class=\"input-group col-md-6\">";
                echo '<span style="color:red;" class="input-group-addon">*</span>';
                  
                  $array = $array_interesting;
                  generate_input($array,  "interesting");

                echo "</div><!--input-group-->";
      }
      ?>
    </div>

    <div class="form-group">
      <span class="col-md-6">Was there any creative input required of you? Did you make something while there? Were you challenged to come up with solutions?</span>
      <div class="col-md-6 input-group">  
        <input class="form-control" type="text" name="survey_innovation" value="" />
      </div>
    </div>

    <div class="form-group">
      <?php
      if ($array_company_size){
        echo "<span class=\"col-md-6\">Company Size:</span>

          <div class=\"input-group col-md-6\">";
            $array = $array_company_size;
              foreach ($array as $number => $item){
                        echo "    
                        <span class=\"input-group-addon\"> 
                        <input type=\"radio\" name=\"survey_company_size\" value=\"". number_format((float)($number+1)/count($array), 2, '.', ''). "\"
      ";
                      
                    echo(">$item</span>");
                    }         
            echo "</div><!-- /input-group -->";
      }
      ?>
      </div>
      
    <div class="form-group">
      <span class="col-md-6">Default Duration of Attachment offered 
        <small>(e.g. 2 months, 12 weeks with possibility of extension, infinite):</small></span>
        <div class="col-md-6 input-group">
          <input class="form-control col-md-6" type="text" name="survey_maximum_duration" value=<?php 
          $survey = $this->session->userdata("survey");
          echo '"'.$survey["survey_maximum_duration"].'"'?> />
        </div>
    </div>
    
    

  <div class="form-group">
      <div class="col-md-offset-6 col-md-6 input-group">
        <input type="submit" class="btn btn btn-next-survey-page col-xs-5"  id="survey_submit_button" name="survey_submit2"  value="SUBMIT" />
        <a id="clear_form_fields" class="btn btn-danger col-xs-offset-2 col-xs-5" >Clear Fields </a>  
      </div>
      <div class="col-md-offset-6 col-md-6 input-group">
        
      </div>
    </div>      
  </form>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $("#clear_form_fields").hide();
    $("#survey_submit_form").submit(function(event){      
      event.preventDefault();
      var dataString = $(this).serialize();
      $("#survey_submit_button").attr("disabled", "disabled");
      // alert(dataString);
      $.post(
        "/index.php/maintenance/survey_data",
        {
          data : dataString
        },
        function(data, status){
          // alert("Done");
          alert(data);
          $("#clear_form_fields").show();
          $("#survey_submit_button").removeAttr("disabled");
      });
    });
    $("#clear_form_fields").click(function(event){
      event.preventDefault();
      $('#survey_submit_form')[0].reset();
      $("#clear_form_fields").hide();
    })
  });
</script>