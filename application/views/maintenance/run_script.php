<div class="container">
	<div class="row text-center">
		<h3><span class="label label-brown">Results</span></h3>
	</div>
	<div><hr></div>
	<div id="script_result" style="overflow:auto;height:18em;visibility:none;">

		<?php
		if ($result){
			foreach ($result as $item){
				foreach ($item as $key => $value){
					echo $key."--->".$value."<br>";
				}
			}
		}
		?>
	</div>

	<form id="run_script_form" role="form" method="POST" action="<?php echo $_SERVER['PHP_SELF']?>">
		<div class="form-group col-xs-12">
			<input id="run_script_input" class="form-control" name="query" cols="6" placeholder="Enter Script e.g. 'alter table meow' then press Enter">
		</div>
		<div class="form-group col-xs-12">
			<input class="btn btn-success col-xs-12 col-md-2 col-md-offset-5" type="submit" value="Run Above">
		</div>
	</form>

	<div>
		<div style="font-weight:bold;">Suggestions</div>
		<div class="list-group">
		<p>
			<ul id="suggestions">
				<a class="list-group-item" href="#">select distinct redo from survey</a>
				<a class="list-group-item" href="#">select * from survey where company_name = ""</a>
			</ul>
		</p>
	</div>


</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#run_script_form").submit(function(event){
			var id = window.setInterval(function() {
			  i = ++i % 4;
			  $("#script_result").html("<div class='text-center'><h1>Loading "+Array(i+2).join(".")+"</h1></div>");
			}, 400);
			
			event.preventDefault();
			var text = $("#run_script_input").val();
			var i = 0;
			

			$.post(
				"/index.php/maintenance/script",
				{
					"query": text,
				},
				function(data, status){
					window.clearInterval(id);
					$("#script_result").html(data);
			});
		});

		$("#suggestions a").click(function(event){
			event.preventDefault();
			var str = $(this).text();
			$("#run_script_input").val(str);
		});
	});
</script>
<!-- footer above this  -->