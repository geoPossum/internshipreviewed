<div class="create_slugs container">
	<table class="table table-condensed">
		<thead>
			<th>Company/ Course</th>
			<th> Slug</th>
		</thead>
		<tbody>
		<?php
		foreach ($result as $inner){
			foreach ($inner as $array){
				if (isset($array['company_name'])){
					echo<<<EOD
					<tr>
						<td>$array[company_name]</td>
						<td>$array[company_slug]</td>
					</tr>
EOD;
				}
				elseif (isset($array['course'])) {
					echo<<<EOD
					<tr>
						<td>$array[course]</td>
						<td>$array[course_slug]</td>
					</tr>
EOD;
				}
		}
		}
		?>
		</tbody>
	</table>