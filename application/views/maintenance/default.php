<?php
$unvalidated_show = $contacted_show = "inline-block";
if (!$unvalidated_badge){
	$unvalidated_show = "none";
}
if (!$contacted_badge){
	$contacted_show = "none";
}
?>
<div class="container">	
	<div class="row maintenance-index">
		<div class="col-xs-12 text-center">
			<h3><span class="label label-default">Welcome to Maintenance</span></h3>
		</div>
	<div role="tabpanel">
		<div class="col-xs-12"> 
			<ul class="nav nav-tabs" role="tablist" id="mytab">
			    <li role="presentation" class="active"><a href="#new" role="tab" data-toggle="tab">New data <span class="badge" style="display:<?php echo $unvalidated_show;?>;"><?php echo $unvalidated_badge; ?></span></a></li>
			    <li role="presentation" class=""><a href="#contacted" role="tab" data-toggle="tab">Contacted <span class="badge" style="display:<?php echo $contacted_show;?>;"><?php echo $contacted_badge; ?></span></a></li>
				<li role="presentation" class=""> <a href="#companies" role="tab" data-toggle="tab">Companies</a></li>
			  	<li role="presentation" class=""> <a href="#courses" role="tab" data-toggle="tab">Courses</a></li>
			  	<li role="presentation" class=""> <a href="#add_slugs" role="tab" data-toggle="tab">Add Slugs</a></li>
				<li role="presentation" class=""> <a href="#add_survey_data" role="tab" data-toggle="tab">Add Survey Data</a></li>
				<li role="presentation" class=""> <a href="#run" role="tab" data-toggle="tab">Run Script</a></li>
			    
			</ul>
		</div>	

		<div class="col-xs-12"> 
			<div class="tab-content">

			    <!-- Panel New Data -->
			    <div role="tabpanel" class="tab-pane  active" id="new">
					<div style="padding-top:1em;">
						<?php $this->load->view("maintenance/unvalidated_companies", array( "companies"=>$unvalidated_items) ); ?>
					</div> <!-- to close company view -->
						
					</div>
			    </div>

			    <!-- Panel Contacted -->
			    <div role="tabpanel" class="tab-pane" id="contacted">
				    	<div class="container">

							<?php
								if ( is_array($contacted_items) and !empty($contacted_items) ){
									foreach($contacted_items as $value){
										if ( is_array($value) ){
											foreach ($value as  $key2 => $value2) {
												if ($key2=="date_solved" || $key2 == 'primary' || $key2 == "sorted" ){
													continue;
												}
												?>
												<div class="row" style="margin-bottom:0.3em;">
													<div class="col-md-3">
														<label><?php echo str_replace("_", " ", ucwords($key2)); ?></label>
													</div>
													<div class="col-md-9">
														<?php echo $value2;?>
													</div>
												</div>
												<?php
											}
										}
										else{
											var_dump($value);
										}
										echo "<br>";
									}
								}
								else{
									?>
									<div class="container">
									<?php 
									echo "No New Contacts";
									?>
									</div>
									<?php
								}

									?>
						</div>
				</div>
			   

			    <!-- Panel Companies -->
			    <div role="tabpanel" class="tab-pane" id="companies">
			    	<div class="container">
			    		<ul class="list-group">
					    	<?php 
					    	foreach ($company_tab_companies as $company) {
					    		echo "<li class='list-group-item'><span style='cursor:pointer;'>$company[company_name]</span></li>";
					    	}
					    	?>
					    </ul>
				    </div>
			    </div>

			    <!-- Panel Courses -->
			    <div role="tabpanel" class="tab-pane" id="courses">
			    	<div class="container">
			    		<ul class="list-group">
					    	<?php 
					    	foreach ($course_tab_courses as $course) {
					    		echo "<li class='list-group-item'><span style='cursor:pointer;'>$course[course]</span></li>";
					    	}
					    	?>
					    </ul>
		    		</div>
			    </div>

			

			    <!-- Panel Add Slugs-->
			    <div role="tabpanel" class="tab-pane" style="padding-top:0.5em; overflow:auto; height:500px;" id="add_slugs">
			    	<div class="container">
		    		</div>
			    </div>

			    <div role="tabpanel" class="tab-pane" style="padding-top:0.5em; overflow:auto; height:500px;" id="add_survey_data">
			    	<div class="container">
			    		<?php 
				    		$this->load->view("maintenance/survey", array(
					    		"array_company_size" => $array_company_size,
					    		"array_days_of_working" => $array_days_of_working,
					    		"array_dress_code" => $array_dress_code,
					    		"array_hands_on" => $array_hands_on,
					    		"array_interesting" => $array_interesting,
					    		"array_redo" => $array_redo
					    		)
				    		); 
			    		?>
		    		</div>
			    </div>

			    <!-- Panel RUN -->
			    <div role="tabpanel" class="tab-pane" id="run">
			    <?php 	if (!isset($result)){
			    	$result = false;
			    }
			    ?>
			    	<?php $this->load->view("maintenance/run_script", array("result"=>$result ) ); ?>
			    </div>
		    </div>
	    </div>
	</div>
	</div>

	<div class="modal fade" id="company_modal"  >
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Change Name of Company</h4>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<div class="col-xs-12" id="former-company-name"></div>
	        	<div class="col-xs-12">
	        		<input class="col-xs-12 form-control" id="new-company-name" type="text" value=""/>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="update_button">Update</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" id="course_modal" >
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Change Name of company</h4>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<div class="col-xs-12" id="former-course-name"></div>
	        	<div class="col-xs-12">
	        		<input class="col-xs-12 form-control" id="new-course-name" type="text" value=""/>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="update_button">Update</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


	<div class="modal" id="success_modal" >
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Success</h4>
	      </div>
	      <div class="modal-body">
	        <div class="row" id="body_content">
	   			Please Be paitent
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div>


<script type="text/javascript">
	function toTitleCase(str)
	{
	    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}

	function update_bg_company(){
		var former = $('#company_modal #former-company-name').text( );
		var newer = $('#company_modal #new-company-name').val( );
		$('#company_modal').modal('hide');
		$('#success_modal .modal-title').text("Running");
		// $('#success_modal #body_content').html("Please Be Patient");
		$('#success_modal').modal('show');
		$.post("/index.php/maintenance/update_company_name",
			{
				"former": former,
				"newer": newer,
			},
			function(data,status){
				original_company_widget.text(newer);
				$('#success_modal').modal('hide');
			});
	}

	function update_bg_course(){
		var former = $('#course_modal #former-course-name').text( );
		var newer = $('#course_modal #new-course-name').val( );
		$('#course_modal').modal('hide');
		$('#success_modal .modal-title').text("Running");
		// $('#success_modal #body_content').html("Please Be Patient");
		$('#success_modal').modal('show');
		$.post("/index.php/maintenance/update_course_name",
			{
				"former": former,
				"newer": newer,
			},
			function(data,status){
				original_course_widget.text(newer);
				$('#success_modal').modal('hide');
			});
	}

	var original_company_widget;
	$("#companies li span").click(function(event){
		event.preventDefault();
		original_company_widget = $(this);
		$('#company_modal #former-company-name').text( $(this).text() );
		$('#company_modal #new-company-name').val( toTitleCase( $(this).text() ) );
		$('#company_modal').modal();
		$('#company_modal #new-company-name').focus();
	});
	$('#company_modal #update_button').click(update_bg_company);
	$('#company_modal #new-company-name').keypress(function(event){
		if ( event.which == 13) {
			update_bg_company();
		}
	});
	

	var original_course_widget;
	$("#courses li span").click(function(event){
		event.preventDefault();
		original_course_widget = $(this);
		$('#course_modal #former-course-name').text( $(this).text() );
		$('#course_modal #new-course-name').val( toTitleCase( $(this).text() ) );
		$('#course_modal #new-course-name').focus();
		jQuery('#course_modal').modal();
	});
	$('#course_modal #update_button').click(update_bg_course);
	$('#course_modal #new-course-name').keypress(function(event){
		if ( event.which === 13) {
			update_bg_course();
		}
	});
	
	//load slugs again
	$('a[href="#add_slugs"][role="tab"][data-toggle="tab"]').on('shown.bs.tab', function (e) {
		$.get(
			"/index.php/maintenance/generate_slugs",
			function(data, status){
				if (status === "success"){
					$("#add_slugs .container").html(data);
				}
			}
			);
	});  

</script>