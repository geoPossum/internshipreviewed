<!-- called from default 2 -->
<div class="container">
	<?php
	
	if ( empty($companies) ){
		echo "No Company Data";
		return;
	}
	if ( !$this->session->userdata("useremail") ){
		echo "
		<div>
			<h4><span class=\"label label-warning\"><a href=\"/index.php/access/login\">Submit Data</a> to see more data and to access more functionality such as <span style=\"color:black;\">sort by pay</span>, highlight <span style=\"color:black;\">high value companies</span> etc.</span></h4>
		</div>";
		echo $this->session->userdata("priviledge");
	}
	else if ( $this->session->userdata("priviledge")<2 ){
		echo "
		<div>
			<h4><span class=\"label label-warning\"><a href=\"#\">Click here</a> to invite friends on facebook and enable sort by pay.</a></span></h4>
		</div>";
	}
	?>
	<table class="table table-condensed table-bordered table-hover">
		<thead>
			<th><a href="<?php echo current_url();?>">
				Companies <span class="glyphicon glyphicon-sort"></span>
				<?php if ( !$this->session->userdata("useremail") ){ echo "<small>Max. 5 Companies</small>"; }?>
				</a> </th>
			<?php
				if ( is_array($companies) ){
					if ( is_array($companies[0]) ){
						if ( isset($companies[0]["pay_slug"]) && $this->session->userdata("priviledge") ){
							echo "<th><a href=\"$_SERVER[PHP_SELF]?s=p\">Pay <span class=\"glyphicon glyphicon-sort\"></span> </a></th>";
						}
					}
				}
			?>
		</thead>
		<tbody>
	<?php
	foreach ($companies as $value){

		echo "<td><a href=/index.php/";

		//can be attachments or access
		echo $this->uri->segment(1)."/";

		echo "unvalidated_company/".rawurlencode($value["id"]).">".ucwords($value["company_name"])."</a>";
		echo "</td>";
		if ( isset($value["pay_slug"]) && $this->session->userdata("priviledge") ){
			echo "<td>".$value["pay"]."</td>";
		}
		echo "</tr>";
	}

	?>
		</tbody>
	</table>