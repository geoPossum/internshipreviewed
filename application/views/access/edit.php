<div class="container"> <!-- Content area -->
<?php
if ( $this->input->get("s")=="done" ){
	echo "
	<div class=\"row \">
		<div class=\"alert-success alert col-md-offset-5 col-md-2\">
			Updated
		</div>
	</div>
	<hr class=\"col-xs-6 col-xs-offset-3\">
	";
}
?>
<form class="form-horizontal" role="form" method="POST" action="/index.php/access/mydata/<?php echo $this->uri->segment(3);?>/?s=done">
<?php
$count = 1;
if (!isset($user_data)){
	if ( isset($survey_data)){
		$survey_data = $user_data;
	}
}
if ( is_array($user_data)){
	foreach ( $user_data as $item ){
		if ( is_array($item)){
			foreach ($item as $key => $value) {
				if ( $key == "id" | $key == "company_slug" | $key == "course_slug" | $key == "email" | $key == "seen"){
					//these are not to be displayed
					if ( $key == "id" ){
						echo "
	<div style=\"display:none\">
		<input name=\"id\" value=\"$value\">
	</div>"			;
					}
					else if ( $key == "email" ){
						echo "
	<div style=\"display:none\">
		<input name=\"email\" value=\"$value\">
	</div>"			;
					}
					else if ( $key == "company_slug" ){
						echo "
	<div style=\"display:none\">
		<input name=\"company_slug\" value=\"$value\">
	</div>"			;
					}
				}


				else{
					$count+=1;
					echo "
	<div class='form-group'>
		<div class=\"col-md-3\">
			<label>";
					echo "".ucwords(str_replace("_", " ", $key))."</label>
		</div>
	";
					if ( $key=="company_size" | $key=="hands_on" | $key=="interesting" | $key=="redo" | $key=="dress_code" | $key=="working_days" ){
						echo "
						<div class='col-md-7'>
						<fieldset class=\"myinput\">
						";
						if ( $key == "company_size"){
							$array = $array_company_size;
						}
						else if ( $key == "working_days"){
							$array = $array_days_of_working;
						}
						else if ( $key == "dress_code"){
							$array = $array_dress_code;
						}
						else if ( $key == "hands_on"){
							$array = $array_hands_on;
						}
						else if ( $key == "interesting"){
							$array = $array_interesting;
						}
						else if ( $key == "redo"){
							$array = $array_redo;

						}

						foreach ($array as $number => $item){
			                echo "	  
			                <span class=\"input-group-addon\"> 
			                <input type=\"radio\" name=\"$key\" value=\"". number_format((float)($number+1)/count($array), 2, '.', ''). "\"
		";
			                if (  number_format((float)($number+1)/count($array), 2, '.', '') == $value ){
						        	echo " checked";
						    	}
						    	echo(">$item</span>");
			            }
			            echo "
			            </fieldset>
			            </div>";
					}
					else if ( $key == "nature_of_work" | $key == "skills" | $key == "testimony" | $key == "innovation" ){
						echo "
		<div class='col-md-7'>
			<textarea name=\"".$key."\" class=\"myinput form-control\" rows=\"".(strlen($value)/60 + 2)."\" type='text'>".trim($value)."
			</textarea>
		</div>";
					}
					else{
						echo "
		<div class='col-md-7'>
			<input name=\"".$key."\" class=\"myinput form-control\" type='text' value=\"".trim($value)."\">
		</div>";
					}
					echo"
		<div class='col-md-2'>
			<a role=\"button\" class=\"my_toggle btn";
					if ($count<5){
						echo  ' btn-info';
					}
					else if ($count<12){
						echo  ' btn-warning';
					}
					else{
						echo  ' btn-success';
					}
					echo " btn-sm\">
				<span class=\"glyphicon glyphicon-edit\">Edit</span>
			</a>
		</div>
	</div>";
				}
			}
		}
		else{}
	}
}
else{}
?>

	<hr class='col-xs-12'>
	<div class="form-group">
		<input type="submit" name="submit_mydata" value="Submit" class="btn btn-primary col-md-4 col-md-offset-4">
	</div>
	<?php
		// var_dump($user_data);
	?>
</form>

<script type="text/javascript">
	$(document).ready( function(){
		$("input.myinput,textarea.myinput,fieldset.myinput").attr("disabled","true");
		$("a.my_toggle").click(function(){
			$(this).parent("div").prev("div").children("input.myinput,textarea.myinput,fieldset.myinput").removeAttr("disabled");
			$(this).hide("normal");
		});
	});
</script>
<style type="text/css">
	a.my_toggle{
		cursor: pointer;
	}
</style>