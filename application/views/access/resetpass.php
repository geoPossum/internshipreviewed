<div class="container resetpass-page first-element"> 
	<div class="row">
		<div class='col-xs-12'>
			<?php if ( isset($message) && $message ){?>
			<div class="alert alert-<?php echo $message_type; ?>">
				<?php echo $message; ?>
			</div>
			<?php } ?>

			<form role="form" method="POST">
				<input type="hidden" name="submit_hidden" value='1'>
				<div class="form-group col-xs-12">
					<div class=" text-center col-md-6 col-md-offset-3 col-xs-12">
					   	<h4><span class="label label-brown">Resetting password for <?php echo $email; ?></span></h4>
				    </div>
				</div>
				<div class="form-group col-xs-12">
					<div class="col-md-6 col-md-offset-3 col-xs-12">
						<label for="exampleInputPassword1">Password</label>
					    <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
					</div>
			    </div>
				<div class="form-group col-xs-12">
					<div class="col-md-6 col-md-offset-3 col-xs-12">
					    <label for="exampleInputPassword2">Confirm Password</label>
					    <input type="password" class="form-control" id="exampleInputPassword2" name="confirm_password" placeholder="Password">
					</div>
				</div>
				<div class="form-group col-xs-12">
					<div class="col-md-6 col-md-offset-3 col-xs-12">
					  	<input class="col-md-6 col-md-offset-3 btn login-custom" type="submit" name="reset_submit" value="Reset Password">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>