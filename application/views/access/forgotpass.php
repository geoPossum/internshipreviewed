<div class="container resetpass-page first-element"> 
	<div class="row">
		<div class='col-xs-12'>
			<?php if ( isset($message) ){
				if ($message){
			?>
			<div class="alert alert-<?php echo $message_type; ?>">
				<?php echo $message; ?>
			</div>
			<?php
				}
			} 
			?>

			<?php if (!isset($disable_email_field) || !$disable_email_field){
			?>
			<form role="form" method="POST">
				<input type="hidden" name="submit_hidden" value='1'>
				<div class="form-group col-xs-12">
				    <label for="exampleInputEmail1">Email address</label>
				    <input type="email" class="form-control" id="exampleInputEmail1" name="forgot_email" placeholder="Enter email">
				</div>
				<div class="form-group col-xs-12">
				  	<input class="col-md-6 col-md-offset-3 btn login-custom" type="submit" name="forgot_submit" value="Reset Password">
				</div>
			</form>
			<?php
			}
			?>

		</div>
	</div>
</div>