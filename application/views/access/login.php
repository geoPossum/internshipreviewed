<div class="container login-page"> 
	<div class="row">
		<div class='col-md-6 col-xs-12 social-logins'>
			<form role="form" method="POST" action="/index.php/access/login">
				<div class="form-group col-xs-12">
				  	<button class="col-md-6 col-md-offset-3 btn login-google" name="google_auth" value="google" type="submit">
				  		<img src="/bin/images/google.png" alt="" height="30px">
				  		Login using Google
				  	</button>
				</div>
				<div class="form-group col-xs-12">
				  	<button class="col-md-6 col-md-offset-3 btn login-facebook" name="facebook_auth" value="facebook" type="submit">
				  		<img src="/bin/images/facebook.png" alt="" height="30px">
				  		Login using Facebook
				  	</button>
				</div>
			</form>
			<div>
				NB. If you already posted data to the site, your facebook or Google Adress must be the same as that that you used
				for these methods to work.
			</div>
		</div>
		<div class='col-md-6 col-xs-12'>
			<form role="form" method="POST" action="/index.php/access/login">
				<hr class="visible-xs">
				<?php
				if (isset($error_message) && $error_message ){
					echo<<<EOD
					<div class="form-group col-xs-12 alert alert-$error_message_type">
						$error_message
					</div>
EOD;
			}
			?>
				<div class="form-group col-xs-12">
				    <label for="exampleInputEmail1">Email address</label>
				    <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Enter email">
				</div>
				<div class="form-group col-xs-12">
				    <label for="exampleInputPassword1">Password</label>
				    <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
				</div>
				<div class="form-group col-xs-12">
					<input type="hidden" name="submit_hidden" value='1'>
				  	<button class="col-md-6 col-md-offset-3 btn login-custom" type="submit" value="Sign in">Sign in</button>
				</div>
				<div class="form-group col-xs-12">
					<div class="text-center">
						<a href="/index.php/access/forgotpass">Forgot/Reset you password?</a><small>(only if you contributed data)</small>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>